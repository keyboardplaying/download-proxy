= Download proxy
Cyrille Chopelet

:proxy-url: https://proxy.keyboardplaying.org/
:java-version: 17


== About this project

This project is a tool to allow the download of trusted files blocked by a firewall, through redirection or transformation of the download stream.

[CAUTION]
====
*This tool was designed in the sole purpose to help developers work* in environment where security rules might hinder them.
It should be considered a *last resort solution* when no other could be found in accordance with the local rules.
Users bear sole responsibility for their use.
====

TIP: A live version of the application is available at the following address: {proxy-url}


== Contributing

=== Prerequisites

- JDK {java-version}


=== Build and test the application

Compilation and testing is performed using standard Maven processes:

.Running the compilation and test process
[source,bash]
----
./mvnw clean install
----


=== Run the application locally

You can run the following command to execute the application locally:

.Running the application locallyfootnote:[This command will use the Maven wrapper included in the project.]
[source,bash]
----
./mvnw quarkus:dev
----

[TIP]
====
The `quarkus:dev` goal does not prepare the front-end.
To have the interface available, you should run the following command:

[source,bash]
----
./mvnw compile
----

Please note the frontend will _not_ refresh the frontend if you modify it.
====


=== Run a Qodana analysis locally

You can run a Qodana analysis locally, by running the following command:

.Linux (Bash)
[source,bash]
----
docker run --rm -it -p 8080:8080 -v $(pwd)/:/data/project/ -v $(pwd)/results/:/data/results/ jetbrains/qodana-jvm --show-report
----

.Windows (PowerShell)
[source,shell]
----
docker run --rm -it -p 8080:8080 -v ${PWD}/:/data/project/ -v ${PWD}/qodana/results/:/data/results/ jetbrains/qodana-jvm --show-report
----

You can then access the report at http://localhost:8080.


=== Build and test the live, native version

The live version is a Docker image of the Java version (see <<issue-native>>).
You may ensure this build is functional by performing it yourself:

.Linux (Bash)
[source,bash]
----
DOCKER_BUILDKIT=1 docker build . -t dl-proxy
docker run --rm -it -p 8080:8080 dl-proxy
----

.Windows (PowerShell)footnote:[Buildkit is activated by default when building a Docker image via PowerShell.]
[source,shell]
----
docker build . -t dl-proxy
docker run --rm -it -p 8080:8080 dl-proxy
----

TIP: Some Quarkus injection introspections may fail when performed via Lombok.
For this reason, it is recommended to execute this section at least before performing a release.
