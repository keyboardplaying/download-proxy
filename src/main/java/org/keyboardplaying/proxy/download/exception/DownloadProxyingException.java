/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.exception;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * An exception for errors occurring during the download proxying process.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
@Getter
@ToString
public class DownloadProxyingException extends Exception {

    /**
     * An error code to help with investigations.
     *
     * @return the code for the error that caused this exception
     */
    private final String errorCode;

    /**
     * {@code true} if this exception was caused by incorrect client input, {@code false} if the cause was technical.
     *
     * @return {@code true} if this exception was caused by incorrect client input, {@code false} otherwise
     */
    private final boolean causedByClientInput;

    /**
     * Additional message arguments for error message to consumer.
     *
     * @return additional message parts
     */
    private final List<Serializable> messageArgs;

    /**
     * @param message             the detail message (which is saved for later retrieval by the getMessage() method)
     * @param cause               the cause (which is saved for later retrieval by the getCause() method).
     *                            (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
     * @param errorCode           the code for the error that caused this exception
     * @param causedByClientInput {@code true} if this exception was caused by incorrect client input, {@code false} otherwise
     * @param messageArgs         additional message arguments for error message to consumer
     */
    @Builder
    private DownloadProxyingException(
        String message, Throwable cause,
        String errorCode, boolean causedByClientInput,
        @Singular List<Serializable> messageArgs
    ) {
        super(message, cause);
        this.errorCode = errorCode;
        this.causedByClientInput = causedByClientInput;
        this.messageArgs = messageArgs;
    }
}
