/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.exception;

/**
 * The possible error codes for this interface.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
public final class ErrorCode {

    // region 1 - Download API | 01 - Incorrect input
    /**
     * Error when the client makes a request but doesn't provide one mandatory parameter.
     */
    public static final String DLP_IN_MISSING_REQUIRED_PARAMETER = "10101";

    /**
     * Error when the client makes a request but one or several of the parameters are incorrect.
     */
    public static final String DLP_IN_INVALID_PARAMETER_VALUE = "10102";
    // endregion

    // region 1 - Download API | 90 - Technical layer
    /**
     * Error when an error occurs that should never happen.
     */
    public static final String DLP_TECH_UNEXPECTED = "19099";
    // endregion

    // Private constructor to avoid instantiation
    private ErrorCode() {
    }
}
