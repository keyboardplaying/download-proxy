/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.filename.model;

import lombok.Builder;

/**
 * This object holds a simple representation of a file's name and extension.
 * <p>
 * This object is immutable.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@Builder
public record Filename(String name, String extension) {

    /**
     * Checks whether this filename has an extension.
     *
     * @return {@code false} if the filename has an extension, {@code false} otherwise
     */
    public boolean isMissingExtension() {
        return extension == null;
    }

    /**
     * Shortcut method for creating an instance.
     *
     * @param name      the file's name without extension
     * @param extension the file's extension
     * @return the created instance
     */
    public static Filename build(String name, String extension) {
        return Filename.builder()
            .name(name)
            .extension(extension)
            .build();
    }

    /**
     * Shortcut method for creating an instance.
     * <p>
     * This method should be used when no clear extension is known.
     *
     * @param name the file's name without extension
     * @return the created instance
     */
    public static Filename build(String name) {
        return Filename.builder()
            .name(name)
            .build();
    }

    /**
     * Returns the filename and extension as would be displayed on a filesystem (e.g. {@code name.ext}).
     *
     * @return the filename with its extension
     */
    @Override
    public String toString() {
        return extension == null ? name : name + "." + extension;
    }
}
