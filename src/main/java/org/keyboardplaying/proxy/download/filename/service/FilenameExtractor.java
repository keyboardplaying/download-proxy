/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.filename.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.HttpHeaders;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.jbosslog.JBossLog;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.util.stream.TokenizedStringStream;

/**
 * This service is dedicated to the extraction of the filename from a URL connection.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@ApplicationScoped
@JBossLog
public class FilenameExtractor {

    protected static final String DEFAULT_NAME_PREFIX = "dlproxy-";
    private static final String DEFAULT_NAME_TIME_FORMAT = "yyMMdd-HHmmssX";

    private static final Pattern PATTERN_FILENAME_HEADER = Pattern.compile("^filename\\*?=([\"'])?([\\w.\\- ]+?)(?:\\.(\\w+))?\\1$");
    private static final Pattern PATTERN_FILENAME_IN_URL = Pattern.compile("([\\w.\\- ]+)\\.(\\w+)(?:$|[&?#])");

    private static final String HEADER_DELIM_SEPARATOR = ";";
    private static final String HEADER_DELIM_EQUAL = "=";
    private static final String HEADER_FIELD_FILENAME = "filename";
    private static final String HEADER_FIELD_FILENAME_S = "filename*";
    private static final String URL_PATH_SEPARATOR = "/";

    /**
     * Extracts a filename from a {@link URLConnection}.
     *
     * @param connection the connection
     * @return the filename found from the connection
     */
    public Optional<Filename> extractFilename(URLConnection connection) {
        // Try to get the filename from headers
        Optional<Filename> filename = extractFilenameFromHeaders(connection.getHeaderFields());
        if (filename.isEmpty()) {
            // If obtaining the filename from headers was not possible, try to find a name in URL
            filename = extractFilenameFromUrl(connection.getURL());
        }

        return filename;
    }

    // protected to be available in test class
    protected Optional<Filename> extractFilenameFromHeaders(Map<String, List<String>> headers) {
        if (headers.containsKey(HttpHeaders.CONTENT_DISPOSITION)) {
            final List<String> contentDisposition = headers.get(HttpHeaders.CONTENT_DISPOSITION);

            log.debugv("Trying to build file name from Content-Disposition header: '{0}'", contentDisposition);

            /* Extract all possible filenames into a map */
            // Keep filename* and filename separated for prioritisation reasons
            final Map<String, List<String>> filenames = contentDisposition.stream()
                .flatMap(header -> new TokenizedStringStream(header, HEADER_DELIM_SEPARATOR).stream())
                .map(String::trim)
                .filter(header -> header.startsWith(HEADER_FIELD_FILENAME))
                .collect(Collectors.groupingBy(
                    header -> getBeforeDelimiter(header, HEADER_DELIM_EQUAL)
                ));

            /* Search in the map to extract the correct name */
            final Optional<Filename> filename = Stream.of(HEADER_FIELD_FILENAME_S, HEADER_FIELD_FILENAME)
                // Get all lists with filename or filename* keys, remove null list, flatMap
                .map(filenames::get)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                // Extract filename from header
                .map(PATTERN_FILENAME_HEADER::matcher)
                .filter(Matcher::matches)
                .map(matcher -> Filename.build(matcher.group(2), matcher.group(3)))
                // Make sure the stream doesn't fail in case the header is incorrect
                .filter(Objects::nonNull)
                // Get the first one
                .findFirst();

            // Print some info for debug
            if (log.isDebugEnabled()) {
                if (filename.isEmpty()) {
                    log.debugv("No filename could be extracted from Content-Disposition header");
                } else {
                    log.debugv("Extracted filename <{0}> from Content-Disposition header", filename.get());
                }
            }
            return filename;
        }

        return Optional.empty();
    }

    // protected to be available in test
    protected Optional<Filename> extractFilenameFromUrl(URL url) {
        log.debugv("Trying to build file name from URL: '{0}'", url);

        /* Try to find a filename in URL (searching both in path and query) */
        final Matcher matcher = PATTERN_FILENAME_IN_URL.matcher(
            URLDecoder.decode(url.toString(), StandardCharsets.UTF_8)
        );
        if (matcher.find()) {
            final Filename filename = Filename.build(matcher.group(1), matcher.group(2));
            log.debugv("Extracted filename <{0}> from URL parameters.");
            return Optional.of(filename);
        }

        /* No filename was found, just return last path segment */
        final String path = url.getPath();
        if (path.endsWith(URL_PATH_SEPARATOR)) {
            // There's no path segment, return empty
            log.debugv("URL ends with a trailing slash, returning empty.");
            return Optional.empty();
        }

        final String lastPathSegment = path.substring(path.lastIndexOf(URL_PATH_SEPARATOR) + 1);
        log.debugv("Extracted last segment of URL <{0}> as filename.", lastPathSegment);
        return Optional.of(Filename.build(lastPathSegment));
    }

    /**
     * Extrapolates an extension from the connection's content type.
     *
     * @param connection the connection
     * @return the file extension or {@code null} if none could be found
     */
    public String getExtensionFromMimetype(URLConnection connection) {
        final String contentType = connection.getContentType();
        if (contentType != null) {
            try {
                final MimeType mimeType = MimeTypes.getDefaultMimeTypes().forName(
                    getBeforeDelimiter(contentType, HEADER_DELIM_SEPARATOR));
                final String ext = mimeType == null ? "" : mimeType.getExtension();
                if (!ext.isEmpty()) {
                    return ext.substring(1);
                }
            } catch (MimeTypeException e) {
                log.warnv("Invalid content-type <{0}>, not returning an extension.", contentType);
            }
        }

        return null;
    }

    /**
     * Builds a generic filename for files downloaded through the proxy, when the real filename could not be determined.
     *
     * @param connection the connection
     * @return a generic filename
     */
    public Filename makeGenericFilename(URLConnection connection) {
        return Filename.builder()
            .name(
                DEFAULT_NAME_PREFIX
                    + ZonedDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern(DEFAULT_NAME_TIME_FORMAT))
            )
            .extension(getExtensionFromMimetype(connection))
            .build();
    }

    /**
     * Utility method to extract the part of a String before the supplied delimiter.
     * <p/>
     * If the delimiter wasn't found, the whole String is returned.
     *
     * @param str the String
     * @param delim the delimiter
     * @return the part of the String before the delimiter
     */
    // protected to be available in test
    protected static String getBeforeDelimiter(String str, String delim) {
        return new TokenizedStringStream(str, delim).stream()
            .findFirst()
            .map(String::trim)
            // orElseThrow should never happen: in the worst case, the first element is the whole String
            // but an orElse is required to pass from an Optional to the real object
            .orElseThrow(() -> new IllegalStateException(
                "Failed to extract part of String before delim."));
    }
}
