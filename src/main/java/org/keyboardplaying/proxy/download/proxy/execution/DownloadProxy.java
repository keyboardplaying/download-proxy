/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.proxy.execution;

import jakarta.ws.rs.core.HttpHeaders;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.filename.service.FilenameExtractor;
import org.keyboardplaying.proxy.download.transform.service.DownloadTransformer;
import org.keyboardplaying.proxy.util.io.NonClosingOutputStream;

/**
 * This object holds the logic required to proxy a request.
 * <p>
 * It is specific to a {@link DownloadTransformer} and {@link URLConnection}. The service used to extract or generate the filename is
 * application-scoped and provided by the factory.
 *
 * @author Cyrille Chopelet
 * @see DownloadProxyFactory
 * @since 2.1.0
 */
@RequiredArgsConstructor
public class DownloadProxy {

    /**
     * The Content-Length {@link URLConnection#getContentLengthLong()} will return if content length is unknown.
     *
     * @see URLConnection#getContentLengthLong()
     */
    private static final long UNKNOWN_CONTENT_LENGTH = -1L;

    /**
     * A template for building the value to the Content-Disposition header.
     */
    private static final String CONTENT_DISPOSITION_ATTACHMENT = "attachment; filename=\"%s\"";

    private final DownloadTransformer transformer;
    private final URLConnection connection;
    private final FilenameExtractor filenameExtractor;

    /**
     * Reads the {@link java.io.InputStream} from the query and writes the transformed stream to the supplied
     * {@link OutputStream}.
     *
     * @param output           the stream to output the content after transformation
     * @param downloadFilename the name for the downloaded file
     * @throws IOException when an error occurs while reading the input or writing to the output
     */
    public void outputTransformedStream(OutputStream output, Filename downloadFilename) throws IOException {
        // The NonClosingOutputStream won't close the underlying stream, which is why output is required as a resource
        try (output; final NonClosingOutputStream out = new NonClosingOutputStream(output)) {
            appendInput(transformer.getPrefixInputStream(), out);
            transformContent(out, downloadFilename);
            appendInput(transformer.getSuffixInputStream(), out);
        }
    }

    private void appendInput(Supplier<InputStream> supplier, NonClosingOutputStream out) throws IOException {
        if (supplier != null) {
            try (final InputStream in = supplier.get()) {
                in.transferTo(out);
            }
        }
    }

    private void transformContent(NonClosingOutputStream out, Filename downloadFilename) throws IOException {
        try (
            final InputStream input = connection.getInputStream();
            final OutputStream filtered = transformer.getFilteredOutputStream(out, downloadFilename)
        ) {
            input.transferTo(filtered);
        }
    }

    /**
     * Returns the name of the downloaded file, or a generated one if not available.
     *
     * @return a {@link Filename} for the download
     * @see FilenameExtractor
     */
    public Filename getDownloadFilename() {
        return filenameExtractor.extractFilename(connection)
            // If filename is present, try to determine extension if missing
            .map(fName -> {
                if (fName.isMissingExtension()) {
                    return Filename.build(fName.name(), filenameExtractor.getExtensionFromMimetype(connection));
                }
                return fName;
            })
            // If filename not found, use a generic filename
            .orElseGet(() -> filenameExtractor.makeGenericFilename(connection));
    }

    /**
     * Builds a map of headers for the proxied content.
     * <p>
     * The headers should include:
     * <ul>
     *     <li>{@code Content-Type} if it can be determined;</li>
     *     <li>{@code Content-Length} if it can be determined;</li>
     *     <li>{@code Content-Disposition} with the value {@code attachment} and a filename representing the proxied
     *     file.</li>
     * </ul>
     *
     * @param downloadFilename the name for the downloaded file
     * @return the headers to return to the requester of a proxy
     */
    public Map<String, Object> buildHeaders(Filename downloadFilename) {
        final Map<String, Object> headers = new HashMap<>();

        /* Set content type if available */
        getProxiedContentType()
            .ifPresent(proxiedType -> headers.put(HttpHeaders.CONTENT_TYPE, proxiedType));

        /* Set content length if possible */
        getProxiedContentLength()
            .ifPresent(proxiedLength -> headers.put(HttpHeaders.CONTENT_LENGTH, proxiedLength));

        /* Set the filename. */
        headers.put(
            HttpHeaders.CONTENT_DISPOSITION,
            String.format(CONTENT_DISPOSITION_ATTACHMENT, transformer.getFilename(downloadFilename))
        );

        return headers;
    }

    private Optional<String> getProxiedContentType() {
        // The proxy should be able to handle null or empty content types
        return Optional.ofNullable(transformer.getContentType(connection.getContentType()));
    }

    private Optional<Long> getProxiedContentLength() {
        final long originalLength = connection.getContentLengthLong();
        if (originalLength != UNKNOWN_CONTENT_LENGTH) {
            // We know the original length
            return Optional.ofNullable(transformer.getContentLength(originalLength));
        }
        return Optional.empty();
    }
}
