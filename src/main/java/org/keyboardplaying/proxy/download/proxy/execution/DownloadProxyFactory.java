/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.proxy.execution;

import io.quarkus.arc.All;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.keyboardplaying.proxy.download.exception.DownloadProxyingException;
import org.keyboardplaying.proxy.download.exception.ErrorCode;
import org.keyboardplaying.proxy.download.filename.service.FilenameExtractor;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;
import org.keyboardplaying.proxy.download.transform.service.DownloadTransformer;

/**
 * This factory can be injected to build dedicated {@link DownloadProxy} instances.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@ApplicationScoped
public class DownloadProxyFactory {

    /**
     * A map containing which proxy should be used depending on the transformation.
     */
    private final Map<Transformation, DownloadTransformer> proxies;


    /**
     * The object in charge of extracting or building the name of a proxied file.
     */
    private final FilenameExtractor filenameExtractor;

    /**
     * Creates a new instance.
     *
     * @param proxies           the proxies available for transforming downloaded few
     * @param filenameExtractor the service in charge of extracting or building the name of a proxied file
     */
    @Inject
    public DownloadProxyFactory(
        // Don't use Instance here, Quarkus doesn't seem to resolve the beans in that case. List works fine, though.
        @SuppressWarnings("CdiInjectionPointsInspection") @All List<DownloadTransformer> proxies,
        FilenameExtractor filenameExtractor
    ) {
        this.proxies = proxies.stream().collect(Collectors.toMap(
            DownloadTransformer::getTransformation, Function.identity()
        ));
        this.filenameExtractor = filenameExtractor;
    }

    /**
     * Creates a {@link DownloadProxy} to handle a request.
     *
     * @param connection     the connection to the URL
     * @param transformation the transformation to apply to the connection
     * @return the proxifier for the specified proxy and connection
     * @throws DownloadProxyingException if an error occurs while building the transformer
     */
    public DownloadProxy buildProxy(URLConnection connection, Transformation transformation) throws DownloadProxyingException {
        final DownloadTransformer transformer = proxies.get(transformation);
        if (transformer == null) {
            throw DownloadProxyingException.builder()
                .message("No transformer found for transformation <" + transformation + ">")
                .errorCode(ErrorCode.DLP_TECH_UNEXPECTED)
                .causedByClientInput(false)
                .build();
        }

        return new DownloadProxy(transformer, connection, filenameExtractor);
    }
}
