/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.rest;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import java.time.ZonedDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.jbosslog.JBossLog;
import org.keyboardplaying.proxy.download.exception.DownloadProxyingException;
import org.keyboardplaying.proxy.download.rest.dto.ErrorDto;
import org.keyboardplaying.proxy.http.service.HttpHeaderLocaleResolver;
import org.keyboardplaying.proxy.util.i18n.FormattingResourceBundle;

/**
 * Exception mapper in charge of handling exceptions thrown by the download proxy API.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
@Provider
@RequiredArgsConstructor(onConstructor_ = @Inject)
@JBossLog
public class DownloadProxyingExceptionMapper implements ExceptionMapper<DownloadProxyingException> {

    /**
     * The bundle containing the error messages to display that should be sent back to the consumer.
     */
    private static final String BUNDLE_NAME = "error";

    /**
     * The locale resolver that will select the locale applied to the error messages.
     */
    private final HttpHeaderLocaleResolver localeResolver;

    /**
     * The request being replied to.
     */
    @Context
    private HttpHeaders headers;

    /**
     * {@inheritDoc}
     *
     * @param exception {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public Response toResponse(DownloadProxyingException exception) {
        final Status status = exception.isCausedByClientInput() ? Status.BAD_REQUEST : Status.INTERNAL_SERVER_ERROR;
        log.errorv(exception, "Received exception, returning {2} status code. Exception details: {0} ({1})",
            exception.getMessage(), exception, status.getStatusCode());

        final FormattingResourceBundle errorMessages = FormattingResourceBundle.getBundle(
            BUNDLE_NAME, localeResolver.resolveLocale(headers)
        );

        return Response
            .status(status)
            .entity(
                ErrorDto.builder()
                    .errorCode(exception.getErrorCode())
                    .message(
                        errorMessages.getMessage(exception.getErrorCode(), exception.getMessageArgs().toArray())
                    )
                    .timestamp(ZonedDateTime.now())
                    .build()
            )
            .build();
    }
}
