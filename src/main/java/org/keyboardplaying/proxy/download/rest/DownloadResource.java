/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.rest;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import lombok.RequiredArgsConstructor;
import lombok.extern.jbosslog.JBossLog;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.keyboardplaying.proxy.download.exception.DownloadProxyingException;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.proxy.execution.DownloadProxy;
import org.keyboardplaying.proxy.download.proxy.execution.DownloadProxyFactory;
import org.keyboardplaying.proxy.download.rest.dto.ErrorDto;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;
import org.keyboardplaying.proxy.download.url.service.UrlService;
import org.keyboardplaying.proxy.util.log.Logs;

/**
 * This resource is used to transmit a transformed stream from a remote URL.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@Path("/dl")
@Tag(
    name = "File download",
    description = "Resources to allow for the download of files and optionally apply transformations to it."
)
@JBossLog
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class DownloadResource {

    /**
     * The transformation to apply when none was specified.
     */
    private static final Transformation DEFAULT_TRANSFORMATION = Transformation.IDENTITY;

    private final UrlService urlService;
    private final DownloadProxyFactory proxyFactory;

    /**
     * Streams a file specified via a URL.
     *
     * @param urlParam the URL supplied as a query parameter
     * @return the response built from the transformed origin stream
     */
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation(
        summary = "Downloads a file while applying the requested transformation.",
        description = """
            This method calls the supplied URL and streams the response to the caller.

            If a transformation has been specified, it will be applied to the stream before being returned. \
            A reverse transformation will then be required to use the file as expected."""
    )
    @APIResponse(responseCode = "200", description = "The requested file transformed as requested.")
    @APIResponse(
        responseCode = "400", description = "The supplied URL could not be open (eg. missing or invalid).",
        content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDto.class)))
    @APIResponse(
        responseCode = "500", description = "An internal error happened and the server failed to proxy the download.",
        content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDto.class)))
    public Response download(
        @Parameter(description = "The URL of the file to download. Both http and https are supported.", required = true)
        @QueryParam("url") String urlParam,
        @Parameter(description = "The transformation to apply to the downloaded file.")
        @QueryParam("transformation") Transformation transformation
    ) throws DownloadProxyingException {

        /* Log request information */
        if (log.isInfoEnabled()) {
            log.infov(
                "Download requested for URL {0} with transformation {1}",
                Logs.escape(urlParam), transformation
            );
        }

        /* Parse URL */
        final URL url = urlService.controlAndParseUrl(urlParam);
        log.tracev("URL passed the checks");

        try {
            final URLConnection conn = url.openConnection();
            log.trace("Connection opened");
            return buildResponse(conn, transformation == null ? DEFAULT_TRANSFORMATION : transformation);
        } catch (IOException e) {
            final String errorMessage = MessageFormat.format("The supplied URL ({0}) could not be open.", Logs.escape(urlParam));
            log.error(errorMessage, e);
            throw new BadRequestException(errorMessage);
        }
    }

    private Response buildResponse(URLConnection conn, Transformation transformation) throws DownloadProxyingException {
        /* Prepare the proxying */
        log.tracev("Building response for transformation {0}", transformation);
        final DownloadProxy proxy = proxyFactory.buildProxy(conn, transformation);
        final Filename downloadFilename = proxy.getDownloadFilename();
        log.tracev("Extracted filename for download: {0}", downloadFilename);

        /* Build the answer with the transformed content. */
        final ResponseBuilder response = Response.ok(
            (StreamingOutput) streamingOutput -> proxy.outputTransformedStream(streamingOutput, downloadFilename)
        );

        /* Set all headers with computed values. */
        proxy.buildHeaders(downloadFilename).forEach(response::header);

        /* Done! Let's return the response. */
        log.debug("Returning successfully proxied download");
        return response.build();
    }
}
