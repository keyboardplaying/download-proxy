/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.enums;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * Available transformations for the download proxying.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@Schema(
    name = "Transformation",
    description = """
        The possible transformations for a downloaded stream.

        * `IDENTITY`: returns the original stream without transformation.
        * `BASE64`: returns the original stream encoded in base64.
        * `ZIP`: returns the original stream zipped."""
)
public enum Transformation {

    /**
     * A transformation to return the proxied file unchanged.
     */
    IDENTITY,

    /**
     * A transformation to return the proxied file base64-encoded.
     */
    BASE64,

    /**
     * A transformation to return the proxied file zipped.
     */
    ZIP,

    /**
     * A transformation to return the proxied file zipped appended after an image.
     */
    IMAGE_ZIP,

    /**
     * A transformation to masquerade a file as jpeg image.
     */
    FAKE_JPEG
}
