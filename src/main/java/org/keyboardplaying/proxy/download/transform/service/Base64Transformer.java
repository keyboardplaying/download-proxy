/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MediaType;
import java.io.OutputStream;
import java.util.Base64;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;

/**
 * A {@link DownloadTransformer} that returns the original content encoded to base 64.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@ApplicationScoped
public class Base64Transformer implements ExtensionAppendingTransformer {

    /**
     * The extension for Base64-encoded files generated through this Proxy.
     */
    private static final String EXTENSION = "b64";

    /**
     * {@inheritDoc}
     *
     * @return {@link Transformation#IDENTITY}
     */
    @Override
    public Transformation getTransformation() {
        return Transformation.BASE64;
    }

    /**
     * Returns a {@code text/plain} mimetype.
     *
     * @param originalMimeType {@inheritDoc}
     * @return {@code text/plain}
     * @see MediaType#TEXT_PLAIN
     */
    @Override
    public String getContentType(String originalMimeType) {
        return MediaType.TEXT_PLAIN;
    }

    /**
     * {@inheritDoc}
     *
     * @param originalContentLength {@inheritDoc}
     * @return the expected content length of the transformed content
     */
    @Override
    public Long getContentLength(long originalContentLength) {
        return originalContentLength / 3L * 4L + (originalContentLength % 3L == 0L ? 0L : 4L);
    }

    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Returns a {@link Base64}{@code .Encoder}-wrapped {@link OutputStream}
     *
     * @param output           {@inheritDoc}
     * @param originalFilename {@inheritDoc}
     * @return an {@link OutputStream} for encoding the byte data into the specified Base64 encoded format
     * @see Base64#getEncoder()
     */
    @Override
    public OutputStream getFilteredOutputStream(OutputStream output, Filename originalFilename) {
        return Base64.getEncoder().wrap(output);
    }
}
