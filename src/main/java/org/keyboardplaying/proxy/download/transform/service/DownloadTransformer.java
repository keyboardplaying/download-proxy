/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.Supplier;

/**
 * This interface how a download is transformed while being proxied.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
public interface DownloadTransformer {

    /**
     * Returns the transformation this Proxy was designed for.
     *
     * @return the transformation this proxy performs
     */
    Transformation getTransformation();

    /**
     * Returns the mimetype of the download after proxying.
     * <p>
     * Implementations may be able to receive {@code null} as a value. They can also return {@code null} if content type
     * cannot be determined.
     *
     * @param originalMimeType the mimetype before proxying, may be null
     * @return the mimetype of the proxied download after transformation
     */
    String getContentType(String originalMimeType);

    /**
     * Returns the length of the download after proxying, or {@code null} if it can't be computed.
     *
     * @param originalContentLength the content length before proxying
     * @return the length of the transformed content
     */
    Long getContentLength(long originalContentLength);

    /**
     * Returns a filename for the proxied file.
     *
     * @param filename the file name
     * @return a filename for the proxied file
     */
    String getFilename(Filename filename);

    /**
     * Returns an input to add at the beginning of the proxied file.
     * <p>
     * This input <strong>will not</strong> be transformed.
     *
     * @return a prefix for the proxied file
     */
    default Supplier<InputStream> getPrefixInputStream() {
        return null;
    }

    /**
     * Returns an input to add at the end of the proxied file.
     * <p>
     * This input <strong>will not</strong> be transformed.
     *
     * @return a suffix for the proxied file
     */
    default Supplier<InputStream> getSuffixInputStream() {
        return null;
    }

    /**
     * Returns an OutputStream that will perform the transformation upon writing the source to the response.
     *
     * @param output           the stream to output the content to after proxying
     * @param originalFilename the name of the original file
     * @return the filtered output stream, that will perform transformation upon writing
     * @see java.io.FilterOutputStream
     */
    OutputStream getFilteredOutputStream(OutputStream output, Filename originalFilename) throws IOException;
}
