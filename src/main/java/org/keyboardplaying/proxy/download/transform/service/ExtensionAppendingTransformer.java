/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import org.keyboardplaying.proxy.download.filename.model.Filename;

/**
 * A helper for {@link DownloadTransformer} implementations that simply append an extension to the original download name.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
public interface ExtensionAppendingTransformer extends DownloadTransformer {

    char EXT_SEPARATOR = '.';

    /**
     * Returns the extension for files downloaded through this proxy.
     * <p>
     * The extension should not begin with a dot, it will be added automatically.
     *
     * @return this proxy's extension
     */
    String getExtension();

    /**
     * Returns the original filename with the extension specific to this proxy appended.
     *
     * @param filename {@inheritDoc}
     * @return the original filename suffixed with the proxy-specific extension
     * @see #getExtension()
     */
    @Override
    default String getFilename(Filename filename) {
        return filename.toString() + EXT_SEPARATOR + getExtension();
    }
}
