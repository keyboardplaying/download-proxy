/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import jakarta.enterprise.context.ApplicationScoped;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.Supplier;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;
import org.keyboardplaying.proxy.http.constant.MimeType;

/**
 * This proxy wraps the original file by adding JPG magic numbers at the beginning and end of the file.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@ApplicationScoped
public class FakeImageTransformer implements ExtensionAppendingTransformer {

    private static final String EXTENSION = "jpg";

    /**
     * The Jpg magic numbers to be found at the beginning of the file.
     */
    private static final byte[] JPG_MAGIC_NUMBERS_PREFIX = {(byte) 0xFF, (byte) 0xD8, (byte) 0xFF};

    /**
     * The Jpg magic numbers to be found at the end of the file.
     */
    private static final byte[] JPG_MAGIC_NUMBERS_SUFFIX = {(byte) 0xFF, (byte) 0xD9};

    /**
     * The number of bytes being added to the file, for final filesize calculation.
     */
    private static final int JPG_MAGIC_NUMBERS_LENGTH = JPG_MAGIC_NUMBERS_PREFIX.length + JPG_MAGIC_NUMBERS_SUFFIX.length;

    /**
     * {@inheritDoc}
     *
     * @return {@link Transformation#FAKE_JPEG}
     */
    @Override
    public Transformation getTransformation() {
        return Transformation.FAKE_JPEG;
    }

    /**
     * Returns an {@code image/jpeg} mimetype.
     *
     * @param originalMimeType {@inheritDoc}
     * @return {@code image/jpg}
     */
    @Override
    public String getContentType(String originalMimeType) {
        return MimeType.IMAGE_JPEG;
    }

    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Returns the sum of the original filesize and number of added bytes.
     *
     * @param originalContentLength {@inheritDoc}
     * @return {@inheritDoc}
     * @see #JPG_MAGIC_NUMBERS_LENGTH
     */
    @Override
    public Long getContentLength(long originalContentLength) {
        return originalContentLength + JPG_MAGIC_NUMBERS_LENGTH;
    }

    /**
     * Returns an {@link InputStream} with the JPG magic numbers for the beginning of a file.
     *
     * @return the jpeg magic number
     */
    @Override
    public Supplier<InputStream> getPrefixInputStream() {
        return () -> new ByteArrayInputStream(JPG_MAGIC_NUMBERS_PREFIX);
    }

    /**
     * Returns an {@link InputStream} with the JPG magic numbers for the end of a file.
     *
     * @return the jpeg magic number
     */
    @Override
    public Supplier<InputStream> getSuffixInputStream() {
        return () -> new ByteArrayInputStream(JPG_MAGIC_NUMBERS_SUFFIX);
    }

    /**
     * Returns the original {@link OutputStream}.
     *
     * @param output           {@inheritDoc}
     * @param originalFilename {@inheritDoc}
     * @return the provided {@link OutputStream}
     */
    @Override
    public OutputStream getFilteredOutputStream(OutputStream output, Filename originalFilename) {
        return output;
    }
}
