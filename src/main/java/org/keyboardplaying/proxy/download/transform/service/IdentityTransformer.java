/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import jakarta.enterprise.context.ApplicationScoped;
import java.io.OutputStream;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;

/**
 * A {@link DownloadTransformer} that returns the original content unchanged.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@ApplicationScoped
public class IdentityTransformer implements DownloadTransformer {

    /**
     * {@inheritDoc}
     *
     * @return {@link Transformation#IDENTITY}
     */
    @Override
    public Transformation getTransformation() {
        return Transformation.IDENTITY;
    }

    /**
     * Returns the supplied mimetype.
     *
     * @param originalMimeType {@inheritDoc}
     * @return the mimetype passed as an argument
     */
    @Override
    public String getContentType(String originalMimeType) {
        return originalMimeType;
    }

    /**
     * Returns the supplied content length.
     *
     * @param originalContentLength {@inheritDoc}
     * @return the content length passed as an argument
     */
    @Override
    public Long getContentLength(long originalContentLength) {
        return originalContentLength;
    }

    /**
     * Returns the original filename.
     *
     * @param filename {@inheritDoc}
     * @return the original filename
     */
    @Override
    public String getFilename(Filename filename) {
        return filename.toString();
    }

    /**
     * Returns the original {@link OutputStream}.
     *
     * @param output           {@inheritDoc}
     * @param originalFilename {@inheritDoc}
     * @return the provided {@link OutputStream}
     */
    @Override
    public OutputStream getFilteredOutputStream(OutputStream output, Filename originalFilename) {
        return output;
    }
}
