/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.io.InputStream;
import java.util.function.Supplier;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;
import org.keyboardplaying.proxy.http.constant.MimeType;

/**
 * A {@link DownloadTransformer} that zips the original content and appends it to an image. The downloading system should perceive the
 * downloaded file as a (heavy) image, but the archive manager should be able to unzip it.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@ApplicationScoped
public class ImageZipTransformer extends ZipTransformer {

    /**
     * The extension for fake image files generated through this Proxy.
     */
    private static final String EXTENSION = "jpg";

    /**
     * The path to the image prepended to the zip file.
     */
    private final String imagePath;

    /**
     * Creates a new instance.
     *
     * @param imagePath the path to the image that will be used in this transformation.
     */
    @Inject
    public ImageZipTransformer(@ConfigProperty(name = "proxy.transformation.image-zip.image.path") String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * {@inheritDoc}
     *
     * @return {@link Transformation#IMAGE_ZIP}
     */
    @Override
    public Transformation getTransformation() {
        return Transformation.IMAGE_ZIP;
    }

    /**
     * Returns an {@code image/jpeg} mimetype.
     *
     * @param originalMimeType {@inheritDoc}
     * @return {@code image/jpg}
     */
    @Override
    public String getContentType(String originalMimeType) {
        return MimeType.IMAGE_JPEG;
    }

    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Returns the image to append the zipped content to.
     *
     * @return a jpeg image
     */
    @Override
    public Supplier<InputStream> getPrefixInputStream() {
        return () -> getClass().getResourceAsStream(imagePath);
    }
}
