/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import jakarta.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;
import org.keyboardplaying.proxy.http.constant.MimeType;

/**
 * A {@link DownloadTransformer} that zips the original content.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@ApplicationScoped
public class ZipTransformer implements ExtensionAppendingTransformer {

    /**
     * The extension for zip files generated through this Proxy.
     */
    private static final String EXTENSION = "zip";

    /**
     * {@inheritDoc}
     *
     * @return {@link Transformation#ZIP}
     */
    @Override
    public Transformation getTransformation() {
        return Transformation.ZIP;
    }

    /**
     * Returns an {@code application/zip} mimetype.
     *
     * @param originalMimeType {@inheritDoc}
     * @return {@code application/zip}
     */
    @Override
    public String getContentType(String originalMimeType) {
        return MimeType.APPLICATION_ZIP;
    }

    /**
     * Returns {@code null}.
     * <p>
     * It seems impossible to predict the size of the compressed content as it depends on the content itself.
     *
     * @param originalContentLength {@inheritDoc}
     * @return {@code null}
     */
    @Override
    public Long getContentLength(long originalContentLength) {
        return null;
    }

    @Override
    public String getExtension() {
        return EXTENSION;
    }

    /**
     * Returns a {@link ZipOutputStream}.
     *
     * @param output           {@inheritDoc}
     * @param originalFilename {@inheritDoc}, used to create the zip entry
     * @return an {@link OutputStream} for zipping the supplied stream
     * @throws IOException when the creation of the zip entry fails
     * @see ZipOutputStream
     */
    @Override
    public OutputStream getFilteredOutputStream(OutputStream output, Filename originalFilename) throws IOException {
        final ZipOutputStream zip = new ZipOutputStream(output);
        zip.putNextEntry(new ZipEntry(originalFilename.toString()));
        return zip;
    }
}
