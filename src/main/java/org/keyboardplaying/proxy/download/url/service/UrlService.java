/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.url.service;

import io.quarkus.runtime.util.StringUtil;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Set;
import lombok.extern.jbosslog.JBossLog;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.keyboardplaying.proxy.download.exception.DownloadProxyingException;
import org.keyboardplaying.proxy.download.exception.ErrorCode;

/**
 * This service is dedicated to the control and parsing of URLs for download proxying.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@ApplicationScoped
@JBossLog
public class UrlService {

    /**
     * The protocols whitelisted for proxy downloading.
     */
    private static final Collection<String> AUTHORIZED_PROTOCOLS = Set.of("http", "https");

    /**
     * A parameter to determine whether local address should be forbidden.
     * <p>
     * If {@code true}, the addresses will be controlled using {@link #isLocalAddress(URL)}.
     *
     * @see #isLocalAddress(URL)
     */
    // Lombok constructor generation + copyableAnnotations fails with Quarkus; use explicit constructor
    private final boolean forbidLocalUrls;

    /**
     * Creates a new instance.
     *
     * @param forbidLocalUrls {@code true} to control the supplied addresses using {@link #isLocalAddress(URL)}
     */
    @Inject
    public UrlService(@ConfigProperty(name = "proxy.security.localhost.forbid") boolean forbidLocalUrls) {
        this.forbidLocalUrls = forbidLocalUrls;
    }

    /**
     * Takes the supplied input and creates a valid {@link URL}. Throws a {@link DownloadProxyingException} if the
     * supplied URL is {@code null} or not an authorized address
     *
     * @param urlStr the String representation of a URL
     * @return the {@link URL} representation of the supplied parameter
     * @throws DownloadProxyingException if the supplied parameter is empty, cannot be parsed or is not an authorized
     *                                   value
     * @see #isAuthorized(URL)
     */
    public URL controlAndParseUrl(String urlStr) throws DownloadProxyingException {
        /* Check a URL was actually provided */
        if (StringUtil.isNullOrEmpty(urlStr)) {
            throw DownloadProxyingException.builder()
                .message("An empty or null String was provided as a URL.")
                .causedByClientInput(true)
                .errorCode(ErrorCode.DLP_IN_MISSING_REQUIRED_PARAMETER)
                .build();
        }

        /* Parse and control the URL */
        try {
            // Build the URL
            final URL url = new URI(urlStr).toURL();

            // Control the URL is valid
            if (!isAuthorized(url)) {

                // URL is invalid, throw exception
                log.errorv("The supplied URL ({0}) did not pass the validity checks.", urlStr);
                throw DownloadProxyingException.builder()
                    .message(
                        MessageFormat.format("The supplied URL ({0}) was marked as invalid.", urlStr)
                    )
                    .causedByClientInput(true)
                    .errorCode(ErrorCode.DLP_IN_INVALID_PARAMETER_VALUE)
                    .build();
            }

            return url;

        } catch (IllegalArgumentException | URISyntaxException | MalformedURLException e) {
            // URL couldn't be parsed, throw exception
            throw DownloadProxyingException.builder()
                .message(
                    MessageFormat.format("The supplied URL ({0}) could not be parsed into a URL", urlStr)
                )
                .cause(e)
                .causedByClientInput(true)
                .errorCode(ErrorCode.DLP_IN_INVALID_PARAMETER_VALUE)
                .build();
        }
    }

    /**
     * Performs security checks on the supplied URL.
     * <p>
     * The URL must not redirect to a local address and the protocol should be whitelisted.
     *
     * @param url the URL to control
     * @return {@code true} if the URL passes all tests, {@code false} otherwise
     */
    protected boolean isAuthorized(URL url) {
        /* Check if protocol is authorized. */
        if (log.isDebugEnabled()) {
            final boolean authorizedProtocol = isAuthorizedProtocol(url);
            final boolean localAddress = isLocalAddress(url);
            final boolean authorizedAddress = authorizedProtocol
                && !(forbidLocalUrls && localAddress);

            log.debugv(
                "Authorized protocols: {0}, Forbid local addresses: {1}, Local address: {2}, Authorized address: {3}",
                authorizedProtocol, forbidLocalUrls, localAddress, authorizedAddress
            );
            return authorizedAddress;
        } else {
            return isAuthorizedProtocol(url)
                && !(forbidLocalUrls && isLocalAddress(url));
        }
    }

    /**
     * Checks whether the protocol figures in the whitelist.
     *
     * @param url the URL to control
     * @return {@code true} if the protocol is in the whitelist, {@code false} otherwise
     */
    private boolean isAuthorizedProtocol(URL url) {
        return AUTHORIZED_PROTOCOLS.contains(url.getProtocol());
    }

    /**
     * Checks whether the provided address may link to the current host.
     *
     * @param url the URL
     * @return {@code true} if the supplied URL may link to the current machine
     */
    private boolean isLocalAddress(URL url) {
        try {
            final InetAddress inetAddress = InetAddress.getByName(url.getHost());
            return inetAddress.isAnyLocalAddress() || inetAddress.isLoopbackAddress() || inetAddress.isLinkLocalAddress();
        } catch (UnknownHostException e) {
            // It should be safe to assume that an unknown host is not a local host
            log.warnv("<{0}> is not a known host. Assuming it is not local.", url.getHost());
            return false;
        }
    }
}
