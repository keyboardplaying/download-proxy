/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.health.rest;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;

/**
 * A health check resource to check the application's liveness and some build information.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@Liveness
@ApplicationScoped
public class HealthCheckResource implements HealthCheck {

    private final String projectId;
    private final String projectVersion;

    /**
     * Creates a new instance.
     *
     * @param projectId      the project's ID
     * @param projectVersion the project's version
     */
    @Inject
    public HealthCheckResource(
        @ConfigProperty(name = "project.id") String projectId,
        @ConfigProperty(name = "project.version") String projectVersion
    ) {
        this.projectId = projectId;
        this.projectVersion = projectVersion;
    }

    /**
     * {@inheritDoc}
     *
     * @return the current status ({@code UP}), along with the project's ID and version
     */
    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.named("project.info")
            .up()
            .withData("project.id", projectId)
            .withData("project.version", projectVersion)
            .build();
    }
}
