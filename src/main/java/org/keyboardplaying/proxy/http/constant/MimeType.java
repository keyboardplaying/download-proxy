/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.http.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Some mimetypes which are used by the application but not defined in JAX-RS's {@link jakarta.ws.rs.core.MediaType}.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
// Hide default constructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MimeType {

    /**
     * The mimetype for a zip file.
     */
    public static final String APPLICATION_ZIP = "application/zip";

    /**
     * The mimetype for a jpg image.
     */
    public static final String IMAGE_JPEG = "image/jpeg";
}
