/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.http.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.HttpHeaders;
import java.util.List;
import java.util.Locale;

/**
 * A service charged with resolving a locale based on the {@code Accept-Language} header of the request.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
@ApplicationScoped
public class HttpHeaderLocaleResolver {

    /**
     * The default locale when no other can be determined.
     */
    public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;

    /**
     * Returns the locale corresponding to the first specified language of the {@code Accept-Language} header. If the
     * header is not populated, the default locale will be returned instead.
     *
     * @param headers the headers of the request the locale is being resolved for
     * @return the locale for the request
     */
    public Locale resolveLocale(HttpHeaders headers) {
        /* If no Accept-Header is specified, use our default */
        final List<String> rawHeaders = headers.getRequestHeader(HttpHeaders.ACCEPT_LANGUAGE);
        if (rawHeaders == null || rawHeaders.isEmpty()) {
            return DEFAULT_LOCALE;
        }

        /* Otherwise, rely on the header */
        // Use Jax's parsing and ordering, get the first header
        return headers.getAcceptableLanguages()
            .stream().findFirst()
            // Return the default Locale if none (should never fall in this case)
            .orElse(DEFAULT_LOCALE);
    }
}
