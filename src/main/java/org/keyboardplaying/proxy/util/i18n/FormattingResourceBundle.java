/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.i18n;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Utility to handle compound messages in resource bundles.
 * <p>
 * This class
 *
 * @author Cyrille Chopelet
 * @see ResourceBundle
 * @see MessageFormat
 * @since 2.3.0
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class FormattingResourceBundle {

    private final ResourceBundle bundle;
    private final MessageFormat formatter;

    /**
     * Gets a resource bundle using the specified base name and locale.
     *
     * @param baseName the base name of the resource bundle, a fully qualified class name
     * @param locale   the locale for which a resource bundle is desired
     * @return a resource bundle for the given base name and locale
     * @throws NullPointerException     if {@code baseName} or {@code locale} is {@code null}
     * @throws MissingResourceException if no resource bundle for the specified base name can be found
     */
    public static FormattingResourceBundle getBundle(String baseName, Locale locale) {
        return new FormattingResourceBundle(
            ResourceBundle.getBundle(baseName, locale),
            new MessageFormat("", locale)
        );
    }

    /**
     * Gets a string for the given key from this resource bundle, and applies formatting.
     *
     * @param key         the key for the desired string
     * @param messageArgs elements to include in the message if any
     * @return the formatted message
     * @throws NullPointerException     if {@code key} is {@code null}
     * @throws MissingResourceException if no object for the given key can be found
     * @throws ClassCastException       if the object found for the given key is not a string
     */
    public String getMessage(String key, Object... messageArgs) {
        formatter.applyPattern(getMessage(key));
        return formatter.format(messageArgs);
    }

    /**
     * Gets a string for the given key from this resource bundle.
     *
     * @param key the key for the desired string
     * @return the message
     * @throws NullPointerException     if {@code key} is {@code null}
     * @throws MissingResourceException if no object for the given key can be found
     * @throws ClassCastException       if the object found for the given key is not a string
     */
    public String getMessage(String key) {
        return bundle.getString(key);
    }
}
