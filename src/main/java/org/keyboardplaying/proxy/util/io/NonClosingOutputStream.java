/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.io;

import java.io.FilterOutputStream;
import java.io.OutputStream;

/**
 * This {@link FilterOutputStream} does not close the underlying {@link OutputStream} upon call to {@link #close()}.
 * <p>
 * This encapsulation was required in order to concatenate streams. The main stream can thus be filtered, while the
 * other sections won't be.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
public class NonClosingOutputStream extends FilterOutputStream {

    /**
     * Creates an output stream filter built on top of the specified
     * underlying output stream.
     *
     * @param out the underlying output stream to be assigned to
     *            the field {@code this.out} for later use, or
     *            <code>null</code> if this instance is to be
     *            created without an underlying stream.
     */
    public NonClosingOutputStream(OutputStream out) {
        super(out);
    }

    /**
     * Does nothing.
     */
    @Override
    public void close() {
        // Do nothing
    }
}
