/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

/**
 * A utility class for handling special cases when logging.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class Logs {

    /**
     * Escapes some special characters to avoid security vulnerabilities.
     *
     * @param value the value to escape
     * @return the escaped value or {@code null} if the original value was {@code null}
     * @see #doEscape(String)
     */
    public static String escape(String value) {
        return value == null ? null : doEscape(value);
    }

    /**
     * Performs the escaping.
     *
     * <ul>
     *     <li>Line breaks are escaped</li>
     * </ul>
     *
     * @param value the value to escape
     * @return the escaped value
     */
    private static String doEscape(String value) {
        return value.replaceAll("[\r\n]", "");
    }
}
