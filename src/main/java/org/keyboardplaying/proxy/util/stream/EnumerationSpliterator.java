/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.stream;

import java.util.Enumeration;
import java.util.Spliterator;
import java.util.Spliterators.AbstractSpliterator;
import java.util.function.Consumer;

/**
 * A spliterator implementation to use {@linkplain Enumeration Enumerations} in
 * {@linkplain java.util.stream.Stream Streams}.
 * <p/>
 * Code inspired from <a href="https://www.baeldung.com/java-enumeration-to-stream">Baeldung</a>.
 *
 * @author Cyrille Chopelet
 * @since 2.3.1
 */
public class EnumerationSpliterator<T> extends AbstractSpliterator<T> {

    /**
     * The {@link Enumeration} being "streamed."
     */
    private final Enumeration<T> enumeration;

    /**
     * Creates a new {@code EnumerationSpliterator} instance for the supplied {@link Enumeration}.
     *
     * @param enumeration the {@link Enumeration} to stream
     * @param <X>         the type of objects carried by the {@link Enumeration}
     * @return a {@link java.util.Spliterator} to stream the supplied {@code Enumeration}
     */
    public static <X> EnumerationSpliterator<X> of(Enumeration<X> enumeration) {
        return new EnumerationSpliterator<>(Long.MAX_VALUE, Spliterator.ORDERED, enumeration);
    }

    /**
     * Creates a new instance.
     *
     * @param est                       the estimated size of this spliterator if known, otherwise
     *                                  {@code Long.MAX_VALUE}.
     * @param additionalCharacteristics properties of this spliterator's
     *                                  source or elements.  If {@code SIZED} is reported then this
     *                                  spliterator will additionally report {@code SUBSIZED}.
     * @param enumeration               the enumeration
     */
    private EnumerationSpliterator(long est, int additionalCharacteristics, Enumeration<T> enumeration) {
        super(est, additionalCharacteristics);
        this.enumeration = enumeration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        if (enumeration.hasMoreElements()) {
            action.accept(enumeration.nextElement());
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void forEachRemaining(Consumer<? super T> action) {
        while (enumeration.hasMoreElements()) {
            action.accept(enumeration.nextElement());
        }
    }
}
