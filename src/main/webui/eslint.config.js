import prettier from 'eslint-config-prettier';
import js from '@eslint/js';
import svelte from 'eslint-plugin-svelte';
import globals from 'globals';
import ts from 'typescript-eslint';

export default ts.config(
    { ignores: ['package-lock.json', 'build/', '.svelte-kit/'] },
    js.configs.recommended,
    ...ts.configs.recommended,
    ...svelte.configs['flat/recommended'],
    prettier,
    ...svelte.configs['flat/prettier'],
    {
        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.node
            }
        }
    },
    {
        files: ['**/*.svelte'],

        languageOptions: {
            parserOptions: {
                parser: ts.parser
            }
        },

        rules: {
            'svelte/no-at-html-tags': 'off' // Only because we're sure there can be no injection
        }
    },
    {
        files: ['**/*.cjs'],

        rules: {
            '@typescript-eslint/no-require-imports': 'off'
        }
    }
);
