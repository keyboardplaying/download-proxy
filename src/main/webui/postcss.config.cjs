const postcssImport = require('postcss-import');
const purgecss = require('@fullhuman/postcss-purgecss');
const cssnano = require('cssnano');

const isProduction = process.env.NODE_ENVIRONMENT === 'production';

const config = {
    plugins: [
        /* Include postcssImport so imports from node_modules is possible */
        postcssImport,

        // Add purgecss and minification only for prod, be faster in dev
        ...(isProduction
            ? [
                  /* Purge unused CSS rules */
                  purgecss({
                      content: ['src/**/*.html', 'src/**/*.svelte']
                  }),

                  /* Minify CSS */
                  cssnano({ preset: 'default' })
              ]
            : [])
    ]
};

module.exports = config;
