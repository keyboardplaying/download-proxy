import Transformation from '../type/Transformation';

/**
 * The list of all transformations available in the download proxy.
 */
// Declare and build the list
const transformations: Transformation[] = [
    new Transformation(
        'IDENTITY',
        "Don't transform file (redirect)",
        `
            <p>
            The downloaded file will not be altered in any way. The only
            difference with a direct download will be the file's origin.
            </p>
            <p>
            This solution is the most efficient to bypass blocked domains,
            but it won't help if a file is refused because of its type or
            content.
            </p>`,
        `
            <p>
            The file will not be transformed, no additional procedure will be needed.
            </p>`
    ),

    new Transformation(
        'BASE64',
        'Encode file to base64',
        `
            <p>
            The downloaded file will be returned as a text file.
            Its content will be the original file, encoded to base64.
            </p>
            <p>
            This might help bypass file content controls, but can also trigger
            a time-consuming antivirus analysis. The transformed file will also
            be 33% larger than the original one, which might cause problems if
            your network has a file size limitation.
            </p>`,
        `
            <p>
            You must decode the encoded file. You may use the following commands
            to do so:
            </p><pre><code># On Linux or in Git Bash
base64 -d my-file.ext.b64 > my-file.ext

# On Windows
certutil -decode file.ext.b64 file.ext</code></pre>`
    ),

    new Transformation(
        'ZIP',
        'Zip file',
        `
            <p>
            The proxy will return a zip, containing your download.
            </p>
            <p>
            Many firewall and antivirus software examine the content of zip
            files, so this may not solve the restrictions you might be facing.
            </p>`,
        `
            <p>Unzip the file to obtain the original download.</p>`
    ),

    new Transformation(
        'FAKE_JPEG',
        'Masquerade file as a JPEG image',
        `
            <p>
            The proxy adds JPEG's
            <a href='https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicators'>magic numbers</a>
            at the beginning and end of the file.
            </p>
            <p>
            Most firewall should detect the downloaded file as a JPEG image,
            but some manipulation is required to remove the format indicators
            the proxy added.
            </p>`,
        `
            <p>The following Linux command will remove the added characters</p>
            <pre><code>sed 's/^\\xFF\\xD8\\xFF//g' file.ext.jpg | sed 's/\\xFF\\xD9$//' > file.ext</code></pre>`
    ),

    new Transformation(
        'IMAGE_ZIP',
        'Zip file and hide it in an image',
        `
            <p>
            The file returned will look like an image, and your preview software
            should even be able to display it, but it will actually be a zip.
            Not all archive software will be able to recognize it, though.
            </p>`,
        `
            <p>
            On Windows, you can open the image as a zip file with software
            like <a href='https://www.7-zip.org/'>7-Zip</a> or
            <a href='https://www.rarlab.com/download.htm'>WinRAR</a>.
            </p>
            <p>On Linux, you can use the <code>unzip</code> command.</p>`
    )
];

// Export as default
export default transformations;
