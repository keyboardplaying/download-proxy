/**
 * A class representing one of the transformations available in the proxy.
 *
 * @author Cyrille Chopelet
 */
export default class Transformation {
    /**
     * The key to use this transformation in the proxy.
     */
    key: string;

    /**
     * A text to display in the UI.
     */
    displayText: string;

    /**
     * Some HTML paragraphs to describe the way this transformation works.
     */
    descriptionHtml: string;

    /**
     * Some HTML paragraphs to explain how to retrieve the original,
     * untransformed file.
     */
    revertProcedureHtml: string;

    /**
     * Creates a new instance.
     * @param key the transformation's key for use in the API
     * @param displayText a human-readable text for display in the UI
     * @param descriptionHtml the description of this transformation
     * @param revertProcedureHtml a procedure to extract the original file from the transformed download
     */
    constructor(
        key: string,
        displayText: string,
        descriptionHtml: string,
        revertProcedureHtml: string
    ) {
        this.key = key;
        this.displayText = displayText;
        this.descriptionHtml = descriptionHtml;
        this.revertProcedureHtml = revertProcedureHtml;
    }
}
