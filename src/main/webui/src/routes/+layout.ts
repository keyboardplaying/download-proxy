// Since there's no dynamic data here, we can prerender it
// so that it gets served as a static asset in production
// Documentation: https://kit.svelte.dev/docs/page-options#prerender
export const prerender = true;
