/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.filename.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link Filename}.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
public class FilenameTest {

    @Nested
    @DisplayName("build(String)")
    class BuildString {

        @Test
        void should_create_instance_without_extension() {
            /* Prepare */
            final String baseName = UUID.randomUUID().toString();

            /* Execute */
            final Filename filename = Filename.build(baseName);

            /* Control */
            assertThat(filename.name()).isEqualTo(baseName);
            assertThat(filename.extension()).isNull();
            assertThat(filename).hasToString(baseName);
        }
    }

    @Nested
    @DisplayName("build(String, String)")
    class BuildStringString {

        @Test
        void should_create_instance_with_extension() {
            /* Prepare */
            final String baseName = UUID.randomUUID().toString();
            final String extension = UUID.randomUUID().toString().split("-")[0];

            /* Execute */
            final Filename filename = Filename.build(baseName, extension);

            /* Control */
            assertThat(filename.name()).isEqualTo(baseName);
            assertThat(filename.extension()).isEqualTo(extension);
            assertThat(filename).hasToString(String.format("%s.%s", baseName, extension));
        }
    }

    @Nested
    @DisplayName("equals(Object) and hashcode()")
    class EqualsAndHashCode {

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.model.FilenameTest#provideEqualsAndHashCodeTestArguments")
        void should_return_true_when_both_name_and_extension_are_the_same(Filename f1, Filename f2, boolean expectedEq) {
            /* Execute */
            final boolean eq = Objects.equals(f1, f2);

            /* Control */
            assertThat(eq).isEqualTo(expectedEq);
            if (expectedEq) {
                assertThat(f1).hasSameHashCodeAs(f2);
            }
        }
    }

    @Nested
    @DisplayName("toString()")
    class ToString {

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.model.FilenameTest#provideToStringTestArguments")
        void should_return_true_when_both_name_and_extension_are_the_same(Filename filename, String expected) {
            /* Execute & control */
            assertThat(filename).hasToString(expected);
        }
    }

    private static Stream<Arguments> provideEqualsAndHashCodeTestArguments() {
        return Stream.of(
            Arguments.of(Filename.build("alpha", "txt"), Filename.build("alpha", "txt"), true),
            Arguments.of(Filename.build("alpha", "txt"), Filename.build("alpha", "jpg"), false),
            Arguments.of(Filename.build("alpha", "txt"), Filename.build("beta", "txt"), false),
            Arguments.of(Filename.build("alpha", "txt"), Filename.build("alpha"), false),
            Arguments.of(Filename.build("alpha"), Filename.build("alpha"), true),
            Arguments.of(Filename.build("alpha"), Filename.build("gamma", "txt"), false)
        );
    }

    private static Stream<Arguments> provideToStringTestArguments() {
        return Stream.of(
            Arguments.of(Filename.build("alpha", "txt"), "alpha.txt"),
            Arguments.of(Filename.build("beta", "jpg"), "beta.jpg"),
            Arguments.of(Filename.build("", "htaccess"), ".htaccess")
        );
    }
}
