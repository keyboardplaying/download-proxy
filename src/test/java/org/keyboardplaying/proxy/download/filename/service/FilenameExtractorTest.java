/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.filename.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
import org.assertj.core.data.MapEntry;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.keyboardplaying.proxy.download.filename.model.Filename;

/**
 * Test class for {@link FilenameExtractor}.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class FilenameExtractorTest {

    private final FilenameExtractor extractor = new FilenameExtractor();

    /**
     * Tests for the {@link FilenameExtractor#extractFilename(URLConnection)} method.
     *
     * @author Cyrille Chopelet
     */
    @Nested
    @DisplayName("extractFilename(URLConnection)")
    class ExtractFilename {

        private static final String url = "https://any.where/myfile.txt";

        private final FilenameExtractor spy = spy(extractor);

        @Test
        void returns_filename_from_headers_if_available() {
            /* Prepare */
            // Expected filename
            final Filename expected = Filename.build(UUID.randomUUID().toString());
            // Filename from headers
            doReturn(Optional.of(expected)).when(spy).extractFilenameFromHeaders(any());

            /* Execute */
            final Optional<Filename> actual = spy.extractFilename(mockUrlConnectionFromUrl(url));

            /* Control */
            verify(spy, times(1)).extractFilenameFromHeaders(any());
            verify(spy, never()).extractFilenameFromUrl(any());
            assertThat(actual).hasValue(expected);
        }

        @Test
        void returns_filename_from_url_if_unavailable_in_headers() {
            /* Prepare */
            // Expected filename
            final Filename expected = Filename.build(UUID.randomUUID().toString());
            // Filename from headers
            doReturn(Optional.empty()).when(spy).extractFilenameFromHeaders(any());
            // Filename from URL
            doReturn(Optional.of(expected)).when(spy).extractFilenameFromUrl(any());

            /* Execute */
            final Optional<Filename> actual = spy.extractFilename(mockUrlConnectionFromUrl(url));

            /* Control */
            verify(spy, times(1)).extractFilenameFromHeaders(any());
            verify(spy, times(1)).extractFilenameFromUrl(any());
            assertThat(actual).hasValue(expected);
        }

        @Test
        void returns_empty_optional_if_no_filename_could_be_found() {
            /* Prepare */
            // Filename from headers
            doReturn(Optional.empty()).when(spy).extractFilenameFromHeaders(any());
            // Filename from URL
            doReturn(Optional.empty()).when(spy).extractFilenameFromUrl(any());

            /* Execute */
            final Optional<Filename> actual = spy.extractFilename(mockUrlConnectionFromUrl(url));

            /* Control */
            verify(spy, times(1)).extractFilenameFromHeaders(any());
            verify(spy, times(1)).extractFilenameFromUrl(any());
            assertThat(actual).isNotNull().isEmpty();
        }
    }

    /**
     * Tests for the {@link FilenameExtractor#extractFilenameFromHeaders(Map)} method.
     *
     * @author Cyrille Chopelet
     */
    @Nested
    @DisplayName("extractFilenameFromHeaders(Map<String, List<String>>)")
    class ExtractFilenameFromHeaders {

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.service.FilenameExtractorTest#provideTestHeadersAndExpectedFilename")
        void extracts_proper_filename_from_headers(Map<String, List<String>> headers, Filename expected) {
            /* Execute */
            final Optional<Filename> result = extractor.extractFilenameFromHeaders(headers);

            /* Control */
            assertThat(result).hasValue(expected);
        }

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.service.FilenameExtractorTest#provideTestHeadersRequiringFallbackToUrl")
        void returns_empty_optional_if_no_valid_header_could_be_found(Map<String, List<String>> headers) {
            /* Execute */
            final Optional<Filename> result = extractor.extractFilenameFromHeaders(headers);

            /* Control */
            assertThat(result).isEmpty();
        }
    }

    /**
     * Tests for the {@link FilenameExtractor#extractFilenameFromUrl(URL)} method.
     *
     * @author Cyrille Chopelet
     */
    @Nested
    @DisplayName("extractFilenameFromUrl(URL)")
    class ExtractFilenameFromUrl {

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.service.FilenameExtractorTest#provideTestUrlsAndExpectedFilenames")
        void should_extract_proper_filename_from_url(URL url, Filename expected) {
            /* Execute */
            final Optional<Filename> result = extractor.extractFilenameFromUrl(url);

            /* Control */
            assertThat(result).hasValue(expected);
        }

        @ParameterizedTest
        @ValueSource(strings = {"https://domain/", "http://domain/context/resource/endpoint/"})
        void should_return_empty_if_url_ends_with_trailing_slash(String url) throws URISyntaxException, MalformedURLException {
            /* Execute */
            final Optional<Filename> result = extractor.extractFilenameFromUrl(new URI(url).toURL());

            /* Control */
            assertThat(result).isEmpty();
        }
    }

    @Nested
    @DisplayName("getExtensionFromMimetype(URLConnection)")
    class GetExtensionFromMimetype {

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.service.FilenameExtractorTest#provideGetExtensionTestArguments")
        void should_return_the_correct_extension_from_mimetype(URLConnection connection, String expected) {
            /* Execute */
            final String extension = extractor.getExtensionFromMimetype(connection);

            /* Control */
            assertThat(extension).isEqualTo(expected);
        }
    }

    @Nested
    @DisplayName("makeGenericFilename(URLConnection)")
    class MakeGenericFilename {

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.download.filename.service.FilenameExtractorTest#provideGetExtensionTestArguments")
        void should_return_a_generic_filename(URLConnection connection, String expectedExt) {
            /* Execute */
            final Filename filename = extractor.makeGenericFilename(connection);

            /* Control */
            assertThat(filename.name()).matches("^dlproxy-\\d{6}-\\d{6}(?:Z|[+-]\\d{4})$");
            assertThat(filename.extension()).isEqualTo(expectedExt);
        }
    }

    @Nested
    @DisplayName("getBeforeDelimiter(String, String)")
    class GetBeforeDelimiter {

        @ParameterizedTest
        @CsvSource({"hello=world,=,hello", "hello|world|here,|,hello"})
        void returns_the_first_part_of_the_tokenized_String(String str, String delim, String expected) {
            /* Execute and control */
            assertThat(FilenameExtractor.getBeforeDelimiter(str, delim)).isEqualTo(expected);
        }

        @Test
        void returns_the_whole_String_if_delimiter_is_not_found() {
            /* Prepare */
            final String str = UUID.randomUUID().toString();

            /* Execute and control */
            assertThat(FilenameExtractor.getBeforeDelimiter(str, "=")).isEqualTo(str);
        }
    }

    // region Header ParameterizedTests arguments
    private static Stream<Arguments> provideTestHeadersAndExpectedFilename() {
        return Map.of(
                List.of("attachment; filename=\"file_name\""),
                Filename.build("file_name"),

                List.of("form-data; name=\"fieldName\"; filename=\"example.txt\""),
                Filename.build("example", "txt"),

                // Test filename* priority, and make sure the order doesn't change the result
                List.of("attachment; filename=\"filename.jpg\"; filename*=\"true-filename.jpg\""),
                Filename.build("true-filename", "jpg"),

                List.of("attachment; filename*=\"true-filename.jpg\"; filename=\"filename.jpg\""),
                Filename.build("true-filename", "jpg"),

                // Test case with several dispositions (should this even happen in real cases?)
                List.of("inline", "attachment; filename=\"filename.jpg\""),
                Filename.build("filename", "jpg")
            )
            .entrySet().stream()
            .map(entry -> Arguments.of(
                Map.of(HttpHeaders.CONTENT_DISPOSITION, entry.getKey())
                , entry.getValue()));
    }

    private static Stream<Arguments> provideTestHeadersRequiringFallbackToUrl() {
        return Stream.of(
            // No Content-Disposition header
            Map.of(),

            // No filename in Content-Disposition
            Map.of(HttpHeaders.CONTENT_DISPOSITION, List.of("inline")),

            // Incorrect filename
            Map.of(HttpHeaders.CONTENT_DISPOSITION, List.of("attachment; filename=\"not:a;legit\\filename\""))
        ).map(Arguments::of);
    }
    // endregion

    // region URL ParameterizedTests arguments
    private static Stream<Arguments> provideTestUrlsAndExpectedFilenames() {
        return Map.of(
                "https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png",
                Filename.build("osi_keyhole_300X300_90ppi_0", "png"),

                "https://quarkus.io/assets/images/quarkus_logo_horizontal_rgb_600px_reverse.png?attribute=value",
                Filename.build("quarkus_logo_horizontal_rgb_600px_reverse", "png"),

                "https://opensource.org/files/osi_keyhole_300X300_90ppi_0.png#anchor",
                Filename.build("osi_keyhole_300X300_90ppi_0", "png"),

                "https://download-proxy/dl?filename=dir%2Florem%20ipsum.tar.gz&transformation=BASE64",
                Filename.build("lorem ipsum.tar", "gz"),

                "http://domain/context/openapi",
                Filename.build("openapi"),

                "http://domain/openapi",
                Filename.build("openapi")
            )
            .entrySet().stream()
            .map(entry -> {
                try {
                    return Arguments.of(new URI(entry.getKey()).toURL(), entry.getValue());
                } catch (URISyntaxException | MalformedURLException e) {
                    throw new IllegalStateException("Test URL <" + entry.getKey() + "> is invalid", e);
                }
            });
    }
    // endregion

    // region Mimetype to extension ParameterizedTests arguments
    private static Stream<Arguments> provideGetExtensionTestArguments() {
        return Stream.of(
            Map.entry(MediaType.TEXT_HTML, "html"),
            Map.entry(MediaType.TEXT_PLAIN, "txt"),
            Map.entry(MediaType.APPLICATION_XML, "xml"),
            Map.entry("application/json;charset=UTF-8", "json"),
            Map.entry("image/jpeg", "jpg"),
            // Use MapEntry.entry here to avoid the NullPointerException on null value
            MapEntry.entry("application/no-one-knows-me", null),
            MapEntry.entry("just:an-invalid_type", null),
            MapEntry.entry((String) null, null)
        ).map(entry -> Arguments.of(mockUrlConnectionFromContentType(entry.getKey()), entry.getValue()));
    }
    // endregion

    // region URL mocks for parameterized tests
    private static URLConnection mockUrlConnectionFromUrl(String url) {
        try {
            final URLConnection connection = mock(URLConnection.class);
            doReturn(Map.of()).when(connection).getHeaderFields();
            doReturn(new URI(url).toURL()).when(connection).getURL();
            return connection;
        } catch (URISyntaxException | MalformedURLException e) {
            // This should never happen, throw an exception to detect it if it does
            throw new RuntimeException(e);
        }
    }

    private static URLConnection mockUrlConnectionFromContentType(String contentType) {
        try {
            final URLConnection connection = mock(URLConnection.class);
            doReturn(new URI("https://download-proxy/url-filename.jpg").toURL()).when(connection).getURL();
            doReturn(contentType).when(connection).getContentType();
            return connection;
        } catch (URISyntaxException | MalformedURLException e) {
            // This should never happen, throw an exception to detect it if it does
            throw new RuntimeException(e);
        }
    }
    // endregion
}
