/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Locale;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.keyboardplaying.proxy.download.exception.DownloadProxyingException;
import org.keyboardplaying.proxy.download.exception.ErrorCode;
import org.keyboardplaying.proxy.download.rest.dto.ErrorDto;
import org.keyboardplaying.proxy.http.service.HttpHeaderLocaleResolver;

/**
 * Test class for {@link DownloadProxyingExceptionMapper}.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class DownloadProxyingExceptionMapperTest {

    /**
     * Locale resolver for tests, always returns the English locale.
     */
    private final HttpHeaderLocaleResolver localeResolver = new HttpHeaderLocaleResolver() {
        @Override
        public Locale resolveLocale(HttpHeaders headers) {
            return Locale.ENGLISH;
        }
    };

    /**
     * The mapper being tested.
     */
    private final DownloadProxyingExceptionMapper exceptionMapper = new DownloadProxyingExceptionMapper(localeResolver);

    @Nested
    @DisplayName("toResponse(DownloadProxyingException)")
    class ToResponse {

        @ParameterizedTest
        @CsvSource({"true,400", "false,500"})
        void sets_http_status_based_on_exception_cause(boolean causedByClient, int httpStatus) {
            /* Prepare */
            final DownloadProxyingException exception = DownloadProxyingException.builder()
                .errorCode(ErrorCode.DLP_IN_MISSING_REQUIRED_PARAMETER)
                .message("Unimportant for this test")
                .causedByClientInput(causedByClient)
                .build();

            /* Execute */
            try (final Response response = exceptionMapper.toResponse(exception)) {

                /* Control */
                assertThat(response.getStatus()).isEqualTo(httpStatus);
            }
        }

        @Test
        void builds_localized_and_formatted_errorMessage() {
            /* Prepare */
            final String parameterName = "someParameter";
            final String parameterValue = "incorrectValue";
            final String errorCode = ErrorCode.DLP_IN_INVALID_PARAMETER_VALUE;
            final DownloadProxyingException exception = DownloadProxyingException.builder()
                .errorCode(errorCode)
                .message("Unimportant for this test")
                .messageArg(parameterName)
                .messageArg(parameterValue)
                .causedByClientInput(true)
                .build();

            /* Execute */
            try (final Response response = exceptionMapper.toResponse(exception)) {

                /* Control */
                final ErrorDto errorDto = (ErrorDto) response.getEntity();
                assertThat(errorDto.errorCode()).isEqualTo(errorCode);
                assertThat(errorDto.message())
                    .isEqualTo("Value <" + parameterValue + "> for parameter <" + parameterName + "> is invalid.");
            }
        }
    }
}
