/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.rest;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;

import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.internal.util.IOUtils;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response.Status;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.tika.Tika;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.keyboardplaying.proxy.download.test.TestFiles;
import org.keyboardplaying.proxy.download.transform.enums.Transformation;
import org.keyboardplaying.proxy.http.constant.MimeType;
import org.keyboardplaying.proxy.test.io.BoundedInputStream;
import org.keyboardplaying.proxy.test.util.Files;

/**
 * This class should make sure the Download resource works as expected.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@QuarkusTest
@TestInstance(Lifecycle.PER_CLASS)
@DisplayNameGeneration(ReplaceUnderscores.class)
class DownloadResourceTest {

    private static final String DOWNLOAD_ENDPOINT = "/dl";

    @TestHTTPResource("/test/lipsum.txt")
    URL textUrl;

    @TestHTTPResource("/test/image.jpg")
    URL imageUrl;

    // region Incorrect queries tests
    @Test
    void dl_should_refuse_request_without_url() {
        // @formatter:off
        given()
            .when().get(DOWNLOAD_ENDPOINT)
            .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
        // @formatter:on
    }

    @ParameterizedTest
    @ValueSource(strings = {" ", "http//test.com", "file://host/path"})
    void dl_should_refuse_request_with_incorrect_or_unauthorized_url(String url) {
        // @formatter:off
        given()
            .queryParam("url", url)
            .when().get(DOWNLOAD_ENDPOINT)
            .then()
                .statusCode(Status.BAD_REQUEST.getStatusCode());
        // @formatter:on
    }
    // endregion

    // region Identity & b64 transformations tests
    @Test
    void dl_without_transformation_should_not_alter_text_file() {
        testDownloadRequest(
            given().queryParam("url", textUrl.toString()),
            "lipsum.txt", MediaType.TEXT_PLAIN
        );
    }

    @Test
    void dl_without_transformation_should_not_alter_binary_file() {
        testDownloadRequest(
            given().queryParam("url", imageUrl.toString()),
            "image.jpg", MimeType.IMAGE_JPEG
        );
    }

    @ParameterizedTest
    @MethodSource("provideTestableTextTransformationArguments")
    void should_download_and_transform_text_request_as_expected(Transformation transformation, String expectedFilename, String expectedType, Supplier<String> expectedContentSupplier) {
        testDownloadRequestAndTransformation(textUrl, transformation, expectedFilename, expectedType, expectedContentSupplier);
    }

    @ParameterizedTest
    @MethodSource("provideTestableBinaryTransformationArguments")
    void should_download_and_transform_binary_request_as_expected(Transformation transformation, String expectedFilename, String expectedType, Supplier<String> expectedContentSupplier) {
        testDownloadRequestAndTransformation(imageUrl, transformation, expectedFilename, expectedType, expectedContentSupplier);
    }

    private void testDownloadRequestAndTransformation(URL url, Transformation transformation, String expectedFilename, String expectedType, Supplier<String> expectedContentSupplier) {
        testDownloadRequest(url, transformation, expectedFilename, expectedType)
            .body(is(expectedContentSupplier.get()));
    }
    // endregion

    // region Zip-based transformations tests
    @Test
    void should_download_and_zip_text_request_as_expected() throws IOException {
        testDownloadRequestAndCompression(textUrl, "lipsum.txt", "lipsum.txt.zip", TestFiles.TEST_ORIG_TEXT_PATH);
    }

    @Test
    void should_download_and_zip_binary_request_as_expected() throws IOException {
        testDownloadRequestAndCompression(imageUrl, "image.jpg", "image.jpg.zip", TestFiles.TEST_ORIG_BIN_PATH);
    }

    private void testDownloadRequestAndCompression(URL url, String originalFilename, String expectedFilename, String originalFilePath) throws IOException {
        final InputStream responseBody = testDownloadRequest(url, Transformation.ZIP, expectedFilename, MimeType.APPLICATION_ZIP)
            .extract().asInputStream();

        /* Control the content of the zip file */
        testZipContent(originalFilename, originalFilePath, responseBody);
    }

    @Test
    void should_download_and_image_zip_text_request_as_expected() throws IOException {
        testDownloadRequestAndImageCompression(textUrl, "lipsum.txt", "lipsum.txt.jpg", TestFiles.TEST_ORIG_TEXT_PATH);
    }

    @Test
    void should_download_and_image_zip_binary_request_as_expected() throws IOException {
        testDownloadRequestAndImageCompression(imageUrl, "image.jpg", "image.jpg.jpg", TestFiles.TEST_ORIG_BIN_PATH);
    }

    private void testDownloadRequestAndImageCompression(URL url, String originalFilename, String expectedFilename, String originalFilePath) throws IOException {
        final InputStream responseBody = testDownloadRequest(url, Transformation.IMAGE_ZIP, expectedFilename, MimeType.IMAGE_JPEG)
            .extract().asInputStream();

        /* Check that the stream begins with the provided image */
        final byte[] image = Files.readTestFileToByteArray("/static/image-n-zip.jpg");
        final byte[] responseBeginning = readNFirstByteOfStream(responseBody, image.length);
        assertThat(responseBeginning).isEqualTo(image);

        /* Control the content of the zip part of the file */
        testZipContent(originalFilename, originalFilePath, responseBody);
    }

    private void testZipContent(String originalFilename, String originalFilePath, InputStream responseBody) throws IOException {
        // Read the zip's first (and only) entry
        try (final ZipInputStream zip = new ZipInputStream(responseBody)) {
            final ZipEntry entry = zip.getNextEntry();
            assertThat(entry).isNotNull();
            assertThat(entry.getName()).isEqualTo(originalFilename);
            final byte[] entryContent = IOUtils.toByteArray(zip);
            assertThat(entryContent)
                .isEqualTo(Files.readTestFileToByteArray(originalFilePath));

            // The zip should contain only one entry
            assertThat(zip.getNextEntry()).isNull();
        }
    }

    private byte[] readNFirstByteOfStream(InputStream input, int length) throws IOException {
        return new BoundedInputStream(input, length).readAllBytes();
    }
    // endregion

    // region Fake jpeg transformation tests
    @Test
    void should_download_and_fake_jpeg_without_altering_original_text() {
        testFakeJpegTransformation(textUrl, "lipsum.txt.jpg", TestFiles.TEST_ORIG_TEXT_PATH);
    }

    @Test
    void should_download_and_fake_jpeg_without_altering_original_binary() {
        testFakeJpegTransformation(imageUrl, "image.jpg.jpg", TestFiles.TEST_ORIG_BIN_PATH);
    }

    private void testFakeJpegTransformation(URL url, String expectedFilename, String originalFile) {
        /* Prepare */
        final byte[] original = Files.readTestFileToByteArray(originalFile);

        /* Execute and control */
        final byte[] responseBody = testDownloadRequest(url, Transformation.FAKE_JPEG, expectedFilename, MimeType.IMAGE_JPEG)
            .header(HttpHeaders.CONTENT_LENGTH, String.valueOf(original.length + 5))
            .extract().asByteArray();

        // Check content is intact and mimetype detection returns jpeg
        assertThat(new Tika().detect(responseBody)).isEqualTo(MimeType.IMAGE_JPEG);
        assertThat(responseBody).contains(original);
    }
    // endregion

    // region Common methods
    private ValidatableResponse testDownloadRequest(URL url, Transformation transformation, String expectedFilename, String expectedType) {
        return testDownloadRequest(
            given()
                .queryParam("url", url.toString())
                .queryParam("transformation", transformation),
            expectedFilename, expectedType
        );
    }

    private ValidatableResponse testDownloadRequest(RequestSpecification requestSpec, String expectedFilename, String expectedType) {
        // @formatter:off
        return requestSpec
            .when()
                .get(DOWNLOAD_ENDPOINT)
            .then()
                .statusCode(Status.OK.getStatusCode())
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + expectedFilename + "\"")
                .contentType(expectedType);
        // @formatter:on
    }
    // endregion

    // region Arguments for parameterized tests
    private static Stream<Arguments> provideTestableTextTransformationArguments() {
        return Stream.of(
            Arguments.of(Transformation.IDENTITY, "lipsum.txt", MediaType.TEXT_PLAIN, (Supplier<String>) () -> Files.readTestFileToString(TestFiles.TEST_ORIG_TEXT_PATH)),
            Arguments.of(Transformation.BASE64, "lipsum.txt.b64", MediaType.TEXT_PLAIN, (Supplier<String>) () -> Files.readBase64EncodedTestFileToString("/encoded/lipsum.b64"))
        );
    }

    private static Stream<Arguments> provideTestableBinaryTransformationArguments() {
        return Stream.of(
            Arguments.of(Transformation.IDENTITY, "image.jpg", MimeType.IMAGE_JPEG, (Supplier<String>) () -> Files.readTestFileToString(TestFiles.TEST_ORIG_BIN_PATH)),
            Arguments.of(Transformation.BASE64, "image.jpg.b64", MediaType.TEXT_PLAIN, (Supplier<String>) () -> Files.readBase64EncodedTestFileToString("/encoded/concrete-wall.b64"))
        );
    }
    // endregion
}
