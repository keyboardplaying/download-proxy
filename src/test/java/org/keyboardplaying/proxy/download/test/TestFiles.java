/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.test;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * The files used for testing the download proxy.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
// Add a private constructor to avoid instantiation
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestFiles {

    /**
     * The relative path to our text sample.
     */
    public static final String TEST_ORIG_TEXT_PATH = "/static/lipsum.txt";

    /**
     * The relative path to our binary file sample.
     */
    public static final String TEST_ORIG_BIN_PATH = "/static/concrete-wall.jpg";
}
