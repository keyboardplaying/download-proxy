/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.test;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Optional;

/**
 * This resource offers some endpoint to download plaintext or binary files to test our proxy endpoint.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@Path("/test")
public class TestResource {

    /**
     * Prints a Lorem Ipsum text to the output.
     *
     * @return a response streaming a Lorem Ipsum
     */
    @GET
    @Path("/lipsum.txt")
    @Produces(MediaType.TEXT_PLAIN)
    public Response printLipsum() {
        return streamFileToResponse(TestFiles.TEST_ORIG_TEXT_PATH);
    }

    /**
     * Prints an image to the output.
     *
     * @return a response streaming an image
     */
    @GET
    @Path("/image.jpg")
    @Produces("image/jpeg")
    public Response printImage() {
        return streamFileToResponse(TestFiles.TEST_ORIG_BIN_PATH);
    }

    /**
     * Builds a response from a {@link StreamingOutput} containing the resource identified by {@code testFilePath}.
     *
     * @param testFilePath the relative path to the file being used as a demo
     * @return the {@link Response} to return as a result from the invocation
     */
    private Response streamFileToResponse(String testFilePath) {
        final java.nio.file.Path path = convertToPath(testFilePath);
        final File file = path.toFile();

        final ResponseBuilder response = Response
            .ok((StreamingOutput) (output) -> {
                try (output; final InputStream input = new FileInputStream(file)) {
                    input.transferTo(output);
                }
            });
        probeContentType(path)
            .ifPresent(contentType -> response.header(HttpHeaders.CONTENT_TYPE, contentType));

        return response
            .header(HttpHeaders.CONTENT_LENGTH, file.length())
            .build();
    }

    private java.nio.file.Path convertToPath(String testFilePath) {
        try {
            return java.nio.file.Path.of(getClass().getResource(testFilePath).toURI());
        } catch (NullPointerException | URISyntaxException e) {
            throw new IllegalStateException("This should not occur.", e);
        }
    }

    private Optional<String> probeContentType(java.nio.file.Path path) {
        String contentType;
        try {
            contentType = Files.probeContentType(path);
        } catch (IOException e) {
            contentType = null;
        }
        return Optional.ofNullable(contentType);
    }
}
