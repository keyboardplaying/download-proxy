/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.execution;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.net.URLConnection;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import jakarta.ws.rs.core.HttpHeaders;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.filename.service.FilenameExtractor;
import org.keyboardplaying.proxy.download.proxy.execution.DownloadProxy;
import org.keyboardplaying.proxy.download.transform.service.DownloadTransformer;

/**
 * Test class for {@link DownloadProxy}.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
public class DownloadProxyTest {

    private static final String PROXIED_FILENAME_EXTENSION = ".proxied";

    private final DownloadTransformer transformer = mock(DownloadTransformer.class);
    private final URLConnection connection = mock(URLConnection.class);
    private final FilenameExtractor extractor = mock(FilenameExtractor.class);
    private final DownloadProxy proxifier = new DownloadProxy(transformer, connection, extractor);

    @Nested
    @DisplayName("getDownloadFilename")
    class GetDownloadFilename {

        @Test
        void should_return_original_filename_when_available() {
            /* Prepare */
            final Filename original = Filename.build(UUID.randomUUID().toString(), "ext");
            doReturn(Optional.of(original)).when(extractor).extractFilename(connection);

            /* Execute */
            final Filename dlFilename = proxifier.getDownloadFilename();

            /* Control */
            assertThat(dlFilename).isEqualTo(original);
        }

        @Test
        void should_try_to_deduce_extension_when_missing_in_original_filename() {
            /* Prepare */
            final String filename = UUID.randomUUID().toString();
            final String extension = "txt";
            doReturn(Optional.of(Filename.build(filename))).when(extractor).extractFilename(connection);
            doReturn(extension).when(extractor).getExtensionFromMimetype(connection);

            /* Execute */
            final Filename dlFilename = proxifier.getDownloadFilename();

            /* Control */
            assertThat(dlFilename)
                .isEqualTo(Filename.build(filename, extension));
        }

        @Test
        void should_generate_default_name_when_missing_from_original() {
            /* Prepare */
            final Filename genericFilename = Filename.build(UUID.randomUUID().toString());
            doReturn(Optional.empty()).when(extractor).extractFilename(connection);
            doReturn(genericFilename).when(extractor).makeGenericFilename(connection);

            /* Execute */
            final Filename dlFilename = proxifier.getDownloadFilename();

            /* Control */
            assertThat(dlFilename).isSameAs(genericFilename);
        }
    }

    @Nested
    @DisplayName("buildHeaders(String)")
    class BuildHeaders {

        @Test
        void should_add_type_header_if_proxy_returns_it_even_if_original_was_empty() {
            /* Prepare */
            final String textPlain = "text/plain; charset=UTF-8";
            doReturn(null).when(connection).getContentType();
            doReturn(textPlain).when(transformer).getContentType(any());

            /* Execute */
            final Map<String, Object> headers = proxifier.buildHeaders(Filename.build(UUID.randomUUID().toString()));

            /* Control */
            assertThat(headers).containsEntry(HttpHeaders.CONTENT_TYPE, textPlain);
        }

        @Test
        void should_not_add_type_header_if_proxy_returns_null() {
            /* Prepare */
            doReturn(null).when(connection).getContentType();
            doReturn(null).when(transformer).getContentType(any());

            /* Execute */
            final Map<String, Object> headers = proxifier.buildHeaders(Filename.build(UUID.randomUUID().toString()));

            /* Control */
            assertThat(headers).doesNotContainKey(HttpHeaders.CONTENT_TYPE);
        }

        @Test
        void should_not_add_length_header_if_not_available_in_original_request() {
            /* Prepare */
            doReturn(-1L).when(connection).getContentLengthLong();

            /* Execute */
            final Map<String, Object> headers = proxifier.buildHeaders(Filename.build(UUID.randomUUID().toString()));

            /* Control */
            assertThat(headers).doesNotContainKey(HttpHeaders.CONTENT_LENGTH);
        }

        @Test
        void should_not_add_length_header_if_not_computable_by_proxy() {
            /* Prepare */
            doReturn(1337L).when(connection).getContentLengthLong();
            doReturn(null).when(transformer).getContentLength(anyLong());

            /* Execute */
            final Map<String, Object> headers = proxifier.buildHeaders(Filename.build(UUID.randomUUID().toString()));

            /* Control */
            assertThat(headers).doesNotContainKey(HttpHeaders.CONTENT_LENGTH);
        }

        @Test
        void should_add_length_header_if_originally_provided_and_computed_by_proxy() {
            /* Prepare */
            final long proxiedLength = new Random().nextLong();
            doReturn(1337L).when(connection).getContentLengthLong();
            doReturn(proxiedLength).when(transformer).getContentLength(anyLong());

            /* Execute */
            final Map<String, Object> headers = proxifier.buildHeaders(Filename.build(UUID.randomUUID().toString()));

            /* Control */
            assertThat(headers).containsEntry(HttpHeaders.CONTENT_LENGTH, proxiedLength);
        }

        @Test
        void should_add_proxied_filename_of_downloadFilename() {
            /* Prepare */
            final Filename filename = Filename.build(UUID.randomUUID().toString(), "ext");
            // When proxying a file name, just add a ".proxied" extension
            doAnswer(
                invocation -> invocation.getArgument(0, Filename.class).toString() + PROXIED_FILENAME_EXTENSION
            ).when(transformer).getFilename(any());

            /* Execute */
            final Map<String, Object> headers = proxifier.buildHeaders(filename);

            /* Control */
            assertThat(headers)
                .hasEntrySatisfying(HttpHeaders.CONTENT_DISPOSITION, new Condition<>(
                    header -> header instanceof String
                        && ((String) header).contains("filename=\"" + filename.toString() + PROXIED_FILENAME_EXTENSION + "\""),
                    "Disposition header contains correct filename"
                ));
        }

    }
}
