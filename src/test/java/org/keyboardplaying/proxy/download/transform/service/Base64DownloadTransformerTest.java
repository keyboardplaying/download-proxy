/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import static org.assertj.core.api.Assertions.assertThat;

import io.quarkus.test.junit.DisabledOnIntegrationTest;
import jakarta.ws.rs.core.MediaType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.UUID;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.keyboardplaying.proxy.download.filename.model.Filename;

/**
 * Test class for {@link Base64Transformer}.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@DisabledOnIntegrationTest
@DisplayNameGeneration(ReplaceUnderscores.class)
class Base64DownloadTransformerTest {

    private final Base64Transformer proxy = new Base64Transformer();

    @Nested
    @DisplayName("getContentType(String)")
    class GetContentType {
        @Test
        void should_always_return_text_plain_content_type() {
            /* Prepare */
            final String original = UUID.randomUUID().toString();

            /* Execute */
            final String contentType = proxy.getContentType(original);

            /* Control */
            assertThat(contentType).isEqualTo(MediaType.TEXT_PLAIN);
        }
    }

    @Nested
    @DisplayName("getContentLength(long)")
    class GetContentLength {

        @ParameterizedTest
        @CsvSource({"0,0", "1,4", "3,4", "4,8"})
        void should_return_correct_expected_length(long original, long expected) {
            /* Execute */
            final Long proxiedLength = proxy.getContentLength(original);

            /* Control */
            assertThat(proxiedLength)
                .isEqualTo(expected);
        }
    }

    @Nested
    @DisplayName("getFilename(String)")
    class GetFilename {

        @Test
        void should_return_a_value_ending_with_b64_and_containing_original_info() {
            /* Prepare */
            final String originalName = "my-filename";
            final String originalExt = "jpg";

            /* Execute */
            final String filename = proxy.getFilename(Filename.build(originalName, originalExt));

            /* Control */
            assertThat(filename)
                .contains(originalName)
                .contains(originalExt)
                .endsWith(".b64");
        }
    }

    @Nested
    @DisplayName("getFilteredOutputStream(output)")
    class GetFilteredOutputStream {

        @Test
        void should_return_a_base64_encoding_OutputStream() throws IOException {
            /* Prepare */
            final String input = UUID.randomUUID().toString();
            final String encoded = Base64.getEncoder().encodeToString(input.getBytes());
            final ByteArrayOutputStream out = new ByteArrayOutputStream();

            /* Execute */
            final OutputStream filtered = proxy.getFilteredOutputStream(out, Filename.build("not-used"));
            filtered.write(input.getBytes());
            filtered.flush();
            filtered.close();

            /* Control */
            assertThat(out).hasToString(encoded);
        }
    }
}
