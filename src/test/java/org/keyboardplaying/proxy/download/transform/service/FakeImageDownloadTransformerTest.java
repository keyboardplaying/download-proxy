/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import org.apache.tika.Tika;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.download.test.TestFiles;
import org.keyboardplaying.proxy.http.constant.MimeType;
import org.keyboardplaying.proxy.test.util.Files;
import org.keyboardplaying.proxy.util.io.NonClosingOutputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link FakeImageTransformer}.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class FakeImageDownloadTransformerTest {

    private final FakeImageTransformer proxy = new FakeImageTransformer();

    @Nested
    @DisplayName("getContentType(String)")
    class GetContentType {
        @Test
        void should_always_return_image_jpeg_content_type() {
            /* Prepare */
            final String original = UUID.randomUUID().toString();

            /* Execute */
            final String contentType = proxy.getContentType(original);

            /* Control */
            assertThat(contentType).isEqualTo(MimeType.IMAGE_JPEG);
        }
    }

    @Nested
    @DisplayName("getContentLength(long)")
    class GetContentLength {

        @ParameterizedTest
        @ValueSource(strings = {TestFiles.TEST_ORIG_TEXT_PATH, TestFiles.TEST_ORIG_BIN_PATH})
        void should_return_transformed_content_s_length(String testFile) throws IOException {
            /* Prepare */
            // Original
            final byte[] originalFile = Files.readTestFileToByteArray(testFile);
            final long originalLength = originalFile.length;
            // Transformed version of file
            final ByteArrayOutputStream transformed = transformFile(originalFile);

            /* Execute */
            final Long proxiedLength = proxy.getContentLength(originalLength);

            /* Control */
            assertThat(proxiedLength).isEqualTo(transformed.size());
        }
    }

    @Nested
    @DisplayName("getFilename(String)")
    class GetFilename {

        @Test
        void should_return_a_value_ending_with_jpg_and_containing_original_info() {
            /* Prepare */
            final String originalName = "my-filename";
            final String originalExt = "jpg";

            /* Execute */
            final String filename = proxy.getFilename(Filename.build(originalName, originalExt));

            /* Control */
            assertThat(filename)
                .contains(originalName)
                .contains(originalExt)
                .endsWith(".jpg");
        }
    }

    @Nested
    @DisplayName("getFilteredOutputStream(output)")
    class GetFilteredOutputStream {

        @Test
        void should_return_a_file_recognized_as_a_jpeg() throws IOException {
            /* Prepare */
            // Original
            final byte[] originalFile = Files.readTestFileToByteArray(TestFiles.TEST_ORIG_TEXT_PATH);

            /* Execute */
            final ByteArrayOutputStream transformed = transformFile(originalFile);

            /* Control */
            assertThat(new Tika().detect(transformed.toByteArray())).isEqualTo(MimeType.IMAGE_JPEG);
            assertThat(transformed.toByteArray()).contains(originalFile);
        }
    }

    private ByteArrayOutputStream transformFile(byte[] originalFile) throws IOException {
        try (
            final ByteArrayOutputStream transformed = new ByteArrayOutputStream();
            final NonClosingOutputStream output = new NonClosingOutputStream(transformed);
            final InputStream main = new ByteArrayInputStream(originalFile);
            final InputStream prefix = proxy.getPrefixInputStream().get();
            final InputStream suffix = proxy.getSuffixInputStream().get();
            final OutputStream filtered = proxy.getFilteredOutputStream(output, Filename.build("lipsum", "txt"))
        ) {
            // Write the image
            prefix.transferTo(output);

            // Transform the input to zip and write it
            main.transferTo(filtered);

            // Write the JPEG magic number
            suffix.transferTo(output);

            // Return the built file
            return transformed;
        }
    }
}
