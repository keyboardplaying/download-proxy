/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.keyboardplaying.proxy.download.filename.model.Filename;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link IdentityTransformer}.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class IdentityTransformerTest {

    private final IdentityTransformer proxy = new IdentityTransformer();

    @Nested
    @DisplayName("getContentType(String)")
    class GetContentType {
        @Test
        void should_return_original_content_type() {
            /* Prepare */
            final String original = UUID.randomUUID().toString();

            /* Execute */
            final String contentType = proxy.getContentType(original);

            /* Control */
            assertThat(contentType).isSameAs(original);
        }
    }

    @Nested
    @DisplayName("getContentLength(long)")
    class GetContentLength {

        @Test
        void should_return_identical_length() {
            /* Prepare */
            final long original = new Random().nextLong();

            /* Execute */
            final Long proxiedLength = proxy.getContentLength(original);

            /* Control */
            assertThat(proxiedLength).isEqualTo(original);
        }
    }

    @Nested
    @DisplayName("getFilename(String)")
    class GetFilename {

        @Test
        void should_return_original_filename() {
            /* Prepare */
            final Filename original = Filename.build(UUID.randomUUID().toString(), "jpg");

            /* Execute */
            final String filename = proxy.getFilename(original);

            /* Control */
            assertThat(filename).isEqualTo(original.toString());
        }
    }

    @Nested
    @DisplayName("getFilteredOutputStream(output)")
    class GetFilteredOutputStream {

        @Test
        void should_return_untransformed_stream() throws IOException {
            /* Prepare */
            final String input = UUID.randomUUID().toString();
            final ByteArrayOutputStream out = new ByteArrayOutputStream();

            /* Execute */
            final OutputStream filtered = proxy.getFilteredOutputStream(out, Filename.build("not-used"));
            filtered.write(input.getBytes());
            filtered.flush();
            filtered.close();

            /* Control */
            assertThat(filtered)
                .isSameAs(out)
                .hasToString(input);
        }
    }
}
