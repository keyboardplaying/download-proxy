/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.transform.service;

import org.apache.tika.Tika;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.keyboardplaying.proxy.download.filename.model.Filename;
import org.keyboardplaying.proxy.http.constant.MimeType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link ImageZipTransformer}.
 *
 * @author Cyrille Chopelet
 * @since 2.1.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class ImageZipDownloadTransformerTest {

    private static final String IMAGE_PATH = "/static/image-n-zip.jpg";

    private final ImageZipTransformer proxy = new ImageZipTransformer(IMAGE_PATH);

    @Nested
    @DisplayName("getContentType(String)")
    class GetContentType {
        @Test
        void should_always_return_image_jpeg_content_type() {
            /* Prepare */
            final String original = UUID.randomUUID().toString();

            /* Execute */
            final String contentType = proxy.getContentType(original);

            /* Control */
            assertThat(contentType).isEqualTo(MimeType.IMAGE_JPEG);
        }
    }

    @Nested
    @DisplayName("getContentLength(long)")
    class GetContentLength {

        @Test
        void should_return_nothing() {
            /* Prepare */
            final long original = new Random().nextLong();

            /* Execute */
            final Long proxiedLength = proxy.getContentLength(original);

            /* Control */
            assertThat(proxiedLength).isNull();
        }
    }

    @Nested
    @DisplayName("getFilename(String)")
    class GetFilename {

        @Test
        void should_return_a_value_ending_with_jpg_and_containing_original_info() {
            /* Prepare */
            final String originalName = "my-filename";
            final String originalExt = "jpg";

            /* Execute */
            final String filename = proxy.getFilename(Filename.build(originalName, originalExt));

            /* Control */
            assertThat(filename)
                .contains(originalName)
                .contains(originalExt)
                .endsWith(".jpg");
        }
    }

    @Nested
    @DisplayName("getPrefixInputStream()")
    class GetPrefixInputStream {

        @Test
        void should_return_a_jpeg_image() throws IOException {
            /* Execute */
            final InputStream prefix = proxy.getPrefixInputStream().get();

            /* Control */
            assertThat(new Tika().detect(prefix)).isEqualTo(MimeType.IMAGE_JPEG);
        }
    }

    @Nested
    @DisplayName("getFilteredOutputStream(output)")
    class GetFilteredOutputStream {

        @Test
        void should_return_a_ZipOutputStream() throws IOException {
            /* Prepare */
            final ByteArrayOutputStream out = new ByteArrayOutputStream();

            /* Execute */
            final OutputStream filtered = proxy.getFilteredOutputStream(out, Filename.build("not-used"));

            /* Control */
            assertThat(filtered).isInstanceOf(ZipOutputStream.class);
        }
    }
}
