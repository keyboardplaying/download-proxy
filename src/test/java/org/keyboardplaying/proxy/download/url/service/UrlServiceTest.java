/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.download.url.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.keyboardplaying.proxy.download.exception.DownloadProxyingException;
import org.keyboardplaying.proxy.download.exception.ErrorCode;

/**
 * Test class for {@link UrlService}.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class UrlServiceTest {

    private final UrlService service = new UrlService(true);

    /**
     * Tests for the {@link UrlService#controlAndParseUrl(String)} method.
     *
     * @author Cyrille Chopelet
     */
    @Nested
    @DisplayName("controlAndParseUrl(String)")
    class ControlAndParseUrl {

        @Test
        void throws_a_DownloadProxyingException_for_null_url() {
            /* Execute and control */
            assertThrowsMissingMandatoryParameterException(null);
        }

        @Test
        void throws_a_DownloadProxyingException_for_empty_url() {
            /* Execute and control */
            assertThrowsMissingMandatoryParameterException("");
        }

        private void assertThrowsMissingMandatoryParameterException(String urlStr) {
            assertThatThrownBy(() -> service.controlAndParseUrl(urlStr))
                .isInstanceOf(DownloadProxyingException.class)
                .hasNoCause()
                .satisfies(exc -> assertThat(((DownloadProxyingException) exc).getErrorCode())
                    .isEqualTo(ErrorCode.DLP_IN_MISSING_REQUIRED_PARAMETER));
        }

        /**
         * This test verifies that several invalid URLs are detected as such.
         * <p>
         * The test values include an invalid protocol ({@code file:}), a local URL or a local IP URL.
         *
         * @param url the invalid URL to be tested
         */
        @ParameterizedTest
        @ValueSource(strings = {
            // Unauthorized protocol
            "file://host/path", "ftp://ftpperso.free.fr/user/directory/file.txt",
            // Local or feedback addresses
            "http://localhost:8080/context", "https://127.0.0.1/sweet/home",
            // Malformed URL
            "this-is-not-a-valid-url"
        })
        void throws_a_DownloadProxyingException_for_forbidden_protocols_and_local_urls(String url) {
            /* Execute and control */
            assertThatThrownBy(() -> service.controlAndParseUrl(url))
                .isInstanceOf(DownloadProxyingException.class)
                .satisfies(exc -> assertThat(((DownloadProxyingException) exc).getErrorCode())
                    .isEqualTo(ErrorCode.DLP_IN_INVALID_PARAMETER_VALUE));
        }

        @Test
        void should_succeed_for_valid_remote_url() throws URISyntaxException, MalformedURLException, DownloadProxyingException {
            /*Prepare */
            final String urlStr = "https://keyboardplaying.org/smoke-tests";

            /* Execute */
            final URL validUrl = service.controlAndParseUrl(urlStr);

            /* Control */
            assertThat(validUrl)
                .isNotNull()
                .isEqualTo(new URI(urlStr).toURL());
        }
    }
}
