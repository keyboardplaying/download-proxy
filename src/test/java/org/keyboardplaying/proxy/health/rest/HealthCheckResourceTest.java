/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.health.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.ws.rs.core.Response.Status;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Test;

/**
 * This class ensures the health check resource works.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@QuarkusTest
@DisplayNameGeneration(ReplaceUnderscores.class)
class HealthCheckResourceTest {

    @Test
    void should_be_up_and_expose_available_data() {
        // @formatter:off
        given()
            .when()
                .get("/q/health/live")
            .then()
                .statusCode(Status.OK.getStatusCode())
                .contentType(ContentType.JSON)
                .body("status", is("UP"))
                .body("checks", hasSize(1))
                .body("checks[0].data", allOf(
                    hasKey("project.id"),
                    hasKey("project.version")
                ));
        // @formatter:on
    }
}
