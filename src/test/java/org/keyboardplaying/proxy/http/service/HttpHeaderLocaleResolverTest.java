/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.http.service;

import static org.assertj.core.api.Assertions.assertThat;

import jakarta.ws.rs.core.HttpHeaders;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.jboss.resteasy.reactive.server.jaxrs.HttpHeadersImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Test class for the {@link HttpHeaderLocaleResolver}.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
public class HttpHeaderLocaleResolverTest {

    private final HttpHeaderLocaleResolver resolver = new HttpHeaderLocaleResolver();

    @Nested
    @DisplayName("resolveLocale(HttpServletRequest)")
    class ResolveLocale {

        @Test
        void returns_the_default_locale_when_Accept_Language_header_is_not_populated() {
            /* Prepare */
            final HttpHeaders headers = new HttpHeadersImpl(List.of());

            /* Execute */
            final Locale locale = resolver.resolveLocale(headers);

            /* Control */
            assertThat(locale).isEqualTo(HttpHeaderLocaleResolver.DEFAULT_LOCALE);
        }

        @ParameterizedTest
        @CsvSource(
            delimiter = '|',
            value = {
                "de|de|''|''", "en-US|en|US|''", "de-DE-1996|de|DE|1996",
                "fr-CH;q=1, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5|fr|CH|''"
            }
        )
        void returns_the_first_available_locale_provided_as_Accept_Language_header(
            String acceptLanguageHeader, String expectedLg, String expectedRegion, String expectedVariant
        ) {
            /* Prepare */
            final List<Map.Entry<String, String>> headerValues = Arrays.stream(acceptLanguageHeader.split(","))
                .map(String::trim)
                .map(value -> new ImmutablePair<>(HttpHeaders.ACCEPT_LANGUAGE, value))
                .collect(Collectors.toList());
            final HttpHeaders headers = new HttpHeadersImpl(headerValues);

            /* Execute */
            final Locale locale = resolver.resolveLocale(headers);

            /* Control */
            assertThat(locale).isEqualTo(
                new Locale.Builder()
                    .setLanguage(expectedLg)
                    .setRegion(expectedRegion)
                    .setVariant(expectedVariant)
                    .build()
            );
        }
    }
}
