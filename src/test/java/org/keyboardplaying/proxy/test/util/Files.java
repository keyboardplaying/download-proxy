/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.test.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * A utility class to read files.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
public class Files {

    /**
     * Reads a file and returns its content as a {@link String}.
     *
     * @param fileRelativePath the path to the file, with its path relative to the caller class
     * @return the content of the file as a string
     */
    public static String readTestFileToString(String fileRelativePath) {
        return new String(readTestFileToByteArray(fileRelativePath));
    }

    /**
     * Reads a file and returns its content as a {@code byte[]}.
     *
     * @param fileRelativePath the path to the file, with its path relative to the caller class
     * @return the content of the file as a byte array
     */
    public static byte[] readTestFileToByteArray(String fileRelativePath) {
        try (final InputStream resourceAsStream = Files.class.getResourceAsStream(fileRelativePath)) {
            if (resourceAsStream == null) {
                throw new IllegalArgumentException(String.format("<%s> could not be read", fileRelativePath));
            }
            return resourceAsStream.readAllBytes();
        } catch (IOException e) {
            throw new IllegalArgumentException(String.format("<%s> could not be read", fileRelativePath));
        }
    }

    /**
     * Reads a file, removes all line breaks and returns it as a {@link String}.
     * <p>
     * This method was designed for OpenSSH-Base64-encoded files.
     *
     * @param fileRelativePath the path to the file, with its path relative to the caller class
     * @return the content of the file as a string
     */
    public static String readBase64EncodedTestFileToString(String fileRelativePath) {
        return readTestFileToString(fileRelativePath).replaceAll("\r?\n", "");
    }
}
