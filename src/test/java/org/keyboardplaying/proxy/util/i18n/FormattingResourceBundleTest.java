/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.i18n;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Date;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Random;
import java.util.regex.Pattern;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Test class for {@link FormattingResourceBundle}.
 *
 * @author Cyrille Chopelet
 * @since 2.3.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class FormattingResourceBundleTest {

    /**
     * The name of the bundle to use for unit tests.
     */
    private static final String BUNDLE_NAME = "i18n/test";

    @Nested
    @DisplayName("getMessage(String)")
    class GetMessage {

        @ParameterizedTest
        @CsvSource({"planet,en,Mars", "planet,eo,Marso"})
        void returns_a_localized_message(String key, String languageTag, String expected) {
            /* Prepare */
            final Locale locale = Locale.forLanguageTag(languageTag);
            final FormattingResourceBundle bundle = FormattingResourceBundle.getBundle(BUNDLE_NAME, locale);

            /* Execute and control */
            assertThat(bundle.getMessage(key)).isEqualTo(expected);
        }

        @Test
        void throws_a_MissingResourceException_if_no_message_could_be_found_for_the_given_key() {
            /* Prepare */
            final FormattingResourceBundle bundle = FormattingResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault());

            /* Execute and control */
            assertThatThrownBy(() -> bundle.getMessage("I don't exist"))
                .isInstanceOf(MissingResourceException.class);
        }
    }

    @Nested
    @DisplayName("getMessage(String, Object...)")
    class GetMessageWithArguments {

        private static final String TEMPLATE_MESSAGE_KEY = "template";
        private static final String PLANET_MESSAGE_KEY = "planet";

        @ParameterizedTest
        @CsvSource(delimiter = '|', value = {
            "en|At 1?\\d:\\d+ [AP]M on [A-Za-z0-9,\\s-]+, we detected \\d+ spaceships on the planet Mars.",
            "eo|Je [0-9:\\sAPM-]+ la [A-Za-z0-9,\\s-]+, ni detektis \\d+ kosmoŝipojn sur la planedo Marso."
        })
        void returns_a_localized_message(String languageTag, String expectedPattern) {
            /* Prepare */
            final Locale locale = Locale.forLanguageTag(languageTag);
            final FormattingResourceBundle bundle = FormattingResourceBundle.getBundle(BUNDLE_NAME, locale);

            /* Execute and control */
            assertThat(
                bundle.getMessage(TEMPLATE_MESSAGE_KEY,
                    bundle.getMessage(PLANET_MESSAGE_KEY), new Random().nextInt(20), new Date()))
                .matches(Pattern.compile("^" + expectedPattern + "$"));
        }

        @Test
        void throws_a_MissingResourceException_if_no_message_could_be_found_for_the_given_key() {
            /* Prepare */
            final FormattingResourceBundle bundle = FormattingResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault());

            /* Execute and control */
            assertThatThrownBy(() -> bundle.getMessage("Nope, not there", "Saturn"))
                .isInstanceOf(MissingResourceException.class);
        }
    }
}
