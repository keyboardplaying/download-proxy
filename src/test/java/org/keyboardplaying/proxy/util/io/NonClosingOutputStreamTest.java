/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.io;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.OutputStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Test class for {@link NonClosingOutputStream}.
 *
 * @author Cyrille Chopelet (CGI)
 * @since 2.1.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
public class NonClosingOutputStreamTest {

    @Nested
    @DisplayName("close()")
    class Close {

        @Test
        void does_not_close_the_underlying_stream() throws IOException {
            /* Prepare */
            final OutputStream underlying = mock(OutputStream.class);
            final NonClosingOutputStream nonClosing = new NonClosingOutputStream(underlying);

            /* Execute */
            nonClosing.close();

            /* Control */
            verify(underlying, never()).close();
        }
    }
}
