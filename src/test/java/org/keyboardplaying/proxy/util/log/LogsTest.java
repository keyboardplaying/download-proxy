/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.log;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link Logs}.
 *
 * @author Cyrille Chopelet
 * @since 2.0.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class LogsTest {

    /**
     * Tests for the {@link Logs#escape(String)} method.
     *
     * @author Cyrille Chopelet
     */
    @Nested
    @DisplayName("escape(String)")
    class EscapeString {

        @Test
        void returns_null_if_parameter_is_null() {
            /* Execute */
            final String escaped = Logs.escape(null);

            /* Control */
            assertThat(escaped).isNull();
        }

        @ParameterizedTest
        @MethodSource("org.keyboardplaying.proxy.util.log.LogsTest#provideStringEscapeTestArguments")
        void returns_original_value_if_no_escape_is_required(String original, String expected) {
            /* Execute */
            final String escaped = Logs.escape(original);

            /* Control */
            assertThat(escaped).isEqualTo(expected);
        }
    }

    private static Stream<Arguments> provideStringEscapeTestArguments() {
        return Stream.of(
            Arguments.of("Hello", "Hello"),
            Arguments.of("Hello\rWorld\n", "HelloWorld")
        );
    }
}
