/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.stream;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Spliterator;
import java.util.StringTokenizer;
import java.util.function.BiConsumer;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link EnumerationSpliterator}.
 *
 * @author Cyrille Chopelet
 * @since 2.4.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class EnumerationSpliteratorTest {

    @Test
    void tryAdvance_iterates_over_the_Enumeration() {
        testSpliteratorConsumption((spliterator, tokens) -> {
            while (spliterator.tryAdvance(tokens::add)) ;
        });
    }

    @Test
    void forEachRemaining_consumes_all_elements_of_the_Enumeration() {
        testSpliteratorConsumption((spliterator, tokens) -> spliterator.forEachRemaining(tokens::add));
    }

    /**
     * Runs the test on both method. The consumer is the method being called for browsing the Enumeration.
     * <p/>
     * The {@link Spliterator} is consumed to feed a list, that will be compared against the expected result.
     *
     * @param consumer a BiConsumer with the tested {@link Spliterator} on the left and the destination list of tokens
     *                 on the right
     */
    void testSpliteratorConsumption(BiConsumer<Spliterator<Object>, List<Object>> consumer) {
        /* Prepare */
        final List<Object> animals = List.of("cat", "dog", "bird");
        final Enumeration<Object> enumeration = new StringTokenizer("cat,dog,bird", ",");
        final Spliterator<Object> spliterator = EnumerationSpliterator.of(enumeration);
        final List<Object> tokens = new ArrayList<>();

        /* Execute */
        consumer.accept(spliterator, tokens);

        /* Control */
        assertThat(tokens).isEqualTo(animals);
    }
}
