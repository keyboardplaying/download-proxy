/*
 * Copyright 2023 Keyboard Playing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.keyboardplaying.proxy.util.stream;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test class for {@link TokenizedStringStream}.
 *
 * @author Cyrille Chopelet
 * @since 2.4.0
 */
@DisplayNameGeneration(ReplaceUnderscores.class)
class TokenizedStringStreamTest {

    @ParameterizedTest
    @CsvSource({";lost;without;a;first;element,;", "Hello!World!,!"})
    void returns_the_same_elements_as_a_StringTokenizer(String str, String delim) {
        /* Prepare */
        final StringTokenizer tokenizer = new StringTokenizer(str, delim);
        final List<String> expected = Collections.list(tokenizer) // Make a list of objects
            .stream().map(String.class::cast) // Map Objects to Strings
            .toList(); // Make a list again

        /* Execute and control */
        assertThat(new TokenizedStringStream(str, delim).stream())
            .isEqualTo(expected);
    }
}
